FROM mcr.microsoft.com/mssql/server:2019-latest

RUN uname -a
RUN whoami

# make netstat available
USER root
RUN apt update
RUN apt install -y net-tools
RUN apt upgrade -y
USER mssql

# Create a config directory
RUN mkdir -p /usr/config
WORKDIR /usr/config

# Bundle config source
COPY docker/. /usr/config
COPY EntireDB-minimal.sql /usr/config/
COPY EntireDB-production.sql /usr/config/

# Grant permissions for our scripts to be executable
USER root
RUN chmod +x /usr/config/entrypoint.sh
RUN chmod +x /usr/config/configure-db.sh
USER mssql

ENV ACCEPT_EULA=Y
ENV SA_PASSWORD=TempBadPassw0rd
RUN ["./configure-db.sh"]

ENTRYPOINT ["./entrypoint.sh"]

HEALTHCHECK --start-period=1m --interval=5s --timeout=10s --retries=30 \
  CMD netstat -an | grep LISTEN | grep 1433

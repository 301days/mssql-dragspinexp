
USE [master]
GO
/*** Close all active connections and drop the DB if it exists - borrowed from http://stackoverflow.com/a/27056858 ***/
WHILE EXISTS(select NULL from sys.databases where name='$(MYDATABASE)')
BEGIN
    DECLARE @SQL varchar(max)
    SELECT @SQL = COALESCE(@SQL,'') + 'Kill ' + Convert(varchar, SPId) + ';'
    FROM MASTER..SysProcesses
    WHERE DBId = DB_ID(N'$(MYDATABASE)') AND SPId <> @@SPId
    EXEC(@SQL)
    DROP DATABASE [$(MYDATABASE)]
END
GO

/****** Object:  Database [$(MYDATABASE)] ******/
CREATE DATABASE [$(MYDATABASE)] 
COLLATE SQL_Latin1_General_CP1_CI_AS
GO
ALTER DATABASE [$(MYDATABASE)] SET COMPATIBILITY_LEVEL = 80
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [$(MYDATABASE)].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [$(MYDATABASE)] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET ANSI_NULLS OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET ANSI_PADDING OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET ARITHABORT OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [$(MYDATABASE)] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [$(MYDATABASE)] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [$(MYDATABASE)] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET  DISABLE_BROKER
GO
ALTER DATABASE [$(MYDATABASE)] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [$(MYDATABASE)] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [$(MYDATABASE)] SET  READ_WRITE
GO
ALTER DATABASE [$(MYDATABASE)] SET RECOVERY SIMPLE
GO
ALTER DATABASE [$(MYDATABASE)] SET  MULTI_USER
GO
ALTER DATABASE [$(MYDATABASE)] SET PAGE_VERIFY TORN_PAGE_DETECTION
GO
ALTER DATABASE [$(MYDATABASE)] SET DB_CHAINING OFF
GO
USE [$(MYDATABASE)]
GO
/****** Object:  User [db1_readonly]     ******/
CREATE USER [db1_readonly] FOR LOGIN [db1_readonly] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  UserDefinedFunction [dbo].[fnTableHasPrimaryKey]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnTableHasPrimaryKey](@sTableName varchar(128))
RETURNS bit
AS
BEGIN
	DECLARE @nTableID int,
		@nIndexID int
	
	SET 	@nTableID = OBJECT_ID(@sTableName)
	
	SELECT 	@nIndexID = indid
	FROM 	sysindexes
	WHERE 	id = @nTableID
	 AND 	indid BETWEEN 1 And 254 
	 AND 	(status & 2048) = 2048
	
	IF @nIndexID IS NOT Null
		RETURN 1
	
	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Select_Field]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a player's name by PlayerID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Account_Select_Field]
	@accountID int,
	@field nvarchar(50) = NULL

AS

EXEC('SELECT ' + @field + ' FROM Account WHERE accountID = ' + @accountID)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Select_Field]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a field by playerID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Select_Field]
	@field nvarchar(50) = NULL,
	@table nvarchar(50) = NULL,
	@playerID int

AS

EXEC('SELECT ' + @field + ' FROM ' + @table + ' WHERE playerID = ' + @playerID)
GO

/****** Object:  Table [dbo].[Zones]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zones](
	[MapName] [varchar](50) NOT NULL,
	[ZoneType] [varchar](50) NOT NULL,
	[Height] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[World]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[World](
	[name] [nvarchar](50) NOT NULL,
	[lottery] [bigint] NOT NULL,
	[days] [nvarchar](max) NULL,
	[seasons] [nvarchar](max) NULL,
	[year] [int] NULL,
	[MAX_SACK] [int] NULL,
	[MAX_LOCKER] [int] NULL,
	[MAX_BELT] [int] NULL,
	[MAX_CHARS] [int] NULL,
	[MAX_UNARMED_WEIGHT] [int] NULL,
	[MAX_MARKS] [int] NULL,
	[rooms] [nvarchar](max) NULL,
	[roomLevels] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[World] ([name], [lottery], [days], [seasons], [year], [MAX_SACK], [MAX_LOCKER], [MAX_BELT], [MAX_CHARS], [MAX_UNARMED_WEIGHT], [MAX_MARKS], [rooms], [roomLevels]) VALUES (N'Test World 00002', 0, N'Nanna Enki Inanna Utu Gugalanna Enlil Ninurta', N'Emesh Enten', 414, 20, 20, 2, 1, 8, 5, N'A~B~C~D~E~F~GM~DEV', N'0~0~0~0~0~0~1~10')
/****** Object:  Table [dbo].[Stores]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Stores](
	[stockID] [int] IDENTITY(1,1) NOT NULL,
	[NPCIdent] [int] NOT NULL,
	[notes] [varchar](max) NULL,
	[original] [bit] NOT NULL,
	[itemID] [int] NOT NULL,
	[sellPrice] [int] NOT NULL,
	[stocked] [smallint] NULL,
	[charges] [smallint] NOT NULL,
	[figExp] [bigint] NOT NULL,
	[seller] [varchar](50) NULL,
	[restock] [smallint] NULL,
 CONSTRAINT [PK_Stores] PRIMARY KEY CLUSTERED 
(
	[stockID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Stores] ON

SET IDENTITY_INSERT [dbo].[Stores] OFF
/****** Object:  Table [dbo].[Spells]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Spells](
	[spellID] [int] NOT NULL,
	[command] [varchar](20) NOT NULL,
	[name] [varchar](255) NOT NULL,
	[description] [nvarchar](max) NOT NULL,
	[methodName] [varchar](255) NOT NULL,
	[beneficial] [bit] NOT NULL,
	[classTypes] [varchar](max) NOT NULL,
	[spellType] [int] NOT NULL,
	[targetType] [int] NOT NULL,
	[requiredLevel] [int] NOT NULL,
	[trainingPrice] [int] NOT NULL,
	[manaCost] [int] NOT NULL,
	[soundFile] [varchar](50) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (1, N'curse', N'Curse', N'The curse spell.', N'castCurse', 0, N'2', 4, 4, 1, 0, 3, N'0225')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (2, N'strength', N'Strength', N'0', N'castStrength', 1, N'2 6', 0, 4, 1, 0, 3, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (3, N'fear', N'Fear', N'0', N'castFear', 0, N'2', 1, 4, 2, 25, 4, N'0229')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (4, N'light', N'Light', N'0', N'castLight', 1, N'2 3 6', 0, 0, 3, 50, 3, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (5, N'blind', N'Blind', N'0', N'castBlind', 0, N'2', 3, 4, 3, 50, 4, N'0229')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (6, N'prfire', N'Protection from Fire', N'0', N'castProtectFire', 1, N'2 3', 0, 4, 3, 75, 4, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (7, N'prcold', N'Protection from Cold', N'0', N'castProtectCold', 1, N'2 3', 0, 4, 4, 100, 5, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (8, N'stun', N'Stun', N'0', N'castStun', 0, N'2', 3, 4, 4, 100, 4, N'0229')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (9, N'cure', N'Cure', N'0', N'castCure', 1, N'2 6', 1, 4, 5, 150, 3, N'0233')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (10, N'neutralize', N'Neutralize Poison', N'0', N'castNeutralizePoison', 1, N'2 5', 1, 4, 5, 450, 4, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (11, N'turnundead', N'Turn Undead', N'0', N'castTurnUndead', 0, N'2', 4, 2, 6, 700, 5, N'0229')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (12, N'lightning', N'Lightning Bolt', N'0', N'castLightningBolt', 0, N'2', 4, 0, 6, 800, 5, N'0066')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (13, N'banish', N'Banish', N'0', N'castBanish', 1, N'2', 4, 4, 7, 6400, 7, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (14, N'death', N'Death', N'0', N'castDeath', 0, N'2', 4, 4, 7, 850, 6, N'0223')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (15, N'raisedead', N'Raise the Dead', N'0', N'castRaiseDead', 1, N'2', 1, 3, 8, 900, 10, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (16, N'resfear', N'Fear Resistance', N'0', N'castResistFear', 1, N'2', 0, 4, 9, 1000, 9, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (17, N'createsnake', N'Create Snake', N'0', N'castCreateSnake', 0, N'2', 2, 0, 10, 1300, 7, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (18, N'summonphantasm', N'Summon Phantasm', N'0', N'castSummonPhantasm', 0, N'2', 2, 2, 11, 1600, 20, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (19, N'resblind', N'Blind Resistance', N'0', N'castResistBlind', 1, N'2', 0, 4, 11, 4000, 11, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (20, N'summondemon', N'Summon Demon', N'0', N'castSummonDemon', 0, N'2', 2, 0, 12, 3200, 14, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (21, N'prfireice', N'Protection from Fire and Ice', N'0', N'castProtectFireIce', 1, N'2 3', 0, 4, 12, 2000, 21, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (22, N'reslightning', N'Lightning Resistance', N'0', N'castResistLightning', 1, N'2', 0, 4, 13, 6000, 13, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (23, N'prpoison', N'Protection from Poison', N'0', N'castProtectPoison', 1, N'2', 0, 4, 14, 12000, 21, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (24, N'poisoncloud', N'Poison Cloud', N'0', N'castPoisonCloud', 0, N'2', 4, 0, 14, 8000, 24, N'0228')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (25, N'resstun', N'Stun Resistance', N'0', N'castResistStun', 1, N'2', 0, 4, 15, 9000, 15, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (26, N'resdeath', N'Death Resistance', N'0', N'castResistDeath', 1, N'2', 0, 4, 16, 8000, 17, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (27, N'prblindfear', N'Protection from Blind and Fear', N'0', N'castProtectBlindFear', 1, N'2', 0, 4, 17, 20000, 27, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (28, N'lightningstorm', N'Lightning Storm', N'0', N'castLightningStorm', 0, N'2 3', 4, 0, 18, 11000, 30, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (29, N'prstundeath', N'Protection from Stun and Death', N'0', N'castProtectStunDeath', 1, N'2', 0, 4, 19, 30000, 34, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (30, N'magicmissile', N'Magic Missile', N'0', N'castMagicMissile', 0, N'3', 4, 4, 1, 0, 3, N'0227')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (31, N'codoor', N'Close and Open Door', N'0', N'castCloseOpenDoor', 0, N'3 5', 1, 4, 2, 50, 3, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (32, N'bonfire', N'Bonfire', N'0', N'castBonfire', 0, N'3', 1, 4, 1, 50, 4, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (33, N'brwater', N'Breathe Water', N'0', N'castBreatheWater', 1, N'3 5', 0, 4, 8, 1100, 8, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (34, N'shield', N'Shield', N'0', N'castShield', 1, N'3', 0, 4, 2, 200, 4, N'0231')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (35, N'darkness', N'Darkness', N'0', N'castDarkness', 0, N'3 5', 1, 0, 4, 400, 5, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (36, N'fsdoor', N'Find Secret Door', N'0', N'castFindSecretDoor', 1, N'3 5', 1, 2, 2, 100, 4, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (37, N'web', N'Create Web', N'0', N'castCreateWeb', 0, N'3', 1, 4, 5, 100, 4, N'0230')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (38, N'portal', N'Create Portal', N'0', N'castPortal', 0, N'3 5', 1, 4, 5, 500, 6, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (39, N'fireball', N'Fireball', N'0', N'castFireBall', 0, N'3', 4, 0, 6, 600, 5, N'0069')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (40, N'firewall', N'Firewall', N'0', N'castFireWall', 0, N'3', 4, 0, 6, 700, 5, N'0224')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (41, N'icestorm', N'Icestorm', N'0', N'castIceStorm', 0, N'3', 4, 0, 7, 800, 6, N'0070')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (42, N'concussion', N'Concussion', N'0', N'castConcussion', 0, N'3', 4, 0, 8, 1000, 10, N'0068')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (43, N'dispel', N'Dispel Illusion', N'0', N'castDispel', 0, N'3', 1, 0, 9, 500, 7, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (44, N'illusion', N'Create Illusion', N'0', N'castIllusion', 0, N'3', 1, 4, 9, 2000, 9, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (45, N'wizardeye', N'Wizard Eye', N'0', N'castWizardEye', 1, N'3 5', 2, 3, 10, 4000, 15, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (46, N'disintegrate', N'Disintegrate', N'0', N'castDisintegrate', 0, N'3', 1, 4, 10, 6000, 12, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (47, N'peek', N'Peek', N'0', N'castPeek', 1, N'3', 3, 3, 11, 10000, 14, N'0226')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (48, N'firebolt', N'Firebolt', N'0', N'castFireBolt', 0, N'3', 4, 4, 12, 5000, 10, N'0224')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (49, N'whirlwind', N'Whirlwind', N'0', N'castWhirlWind', 0, N'3', 4, 0, 13, 7500, 20, N'0072')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (50, N'icespear', N'Icespear', N'0', N'castIceSpear', 0, N'3', 4, 4, 14, 15000, 14, N'0222')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (51, N'firestorm', N'Firestorm', N'0', N'castFireStorm', 0, N'3', 4, 0, 15, 10000, 16, N'0224')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (52, N'drbreath', N'Dragon''s Breath', N'0', N'castDragonsBreath', 0, N'3', 4, 0, 16, 12000, 18, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (53, N'lightninglance', N'Lightning Lance', N'0', N'castLightningLance', 0, N'3', 4, 0, 17, 20000, 20, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (54, N'blizzard', N'Blizzard', N'0', N'castBlizzard', 0, N'3', 4, 0, 18, 25000, 32, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (55, N'createlava', N'Create Lava', N'0', N'castLava', 0, N'3', 4, 0, 19, 30000, 33, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (56, N'hide', N'Hide in Shadows', N'0', N'castHideInShadows', 1, N'5', 3, 3, 1, 0, 3, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (57, N'identify', N'Identify', N'0', N'castIdentify', 1, N'5', 3, 3, 3, 400, 5, N'0226')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (58, N'makerecall', N'Make Recall', N'0', N'castMakeRecall', 1, N'5', 2, 3, 4, 600, 6, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (59, N'nightvision', N'Night Vision', N'0', N'castNightVision', 1, N'5', 1, 3, 6, 1000, 10, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (60, N'hidedoor', N'Hide Door', N'0', N'castHideDoor', 1, N'5', 1, 4, 7, 900, 7, N'0230')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (61, N'featherfall', N'Feather Fall', N'0', N'castFeatherFall', 1, N'5', 1, 4, 9, 1500, 12, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (62, N'speed', N'Speed', N'0', N'castSpeed', 1, N'5', 1, 3, 11, 8000, 20, N'0232')
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (63, N'venom', N'Venom', N'0', N'castVenom', 1, N'5', 2, 3, 13, 10000, 22, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (64, N'bless', N'Blessing of the Faithful', N'0', N'castBless', 1, N'6', 0, 4, 8, 0, 3, NULL)
INSERT [dbo].[Spells] ([spellID], [command], [name], [description], [methodName], [beneficial], [classTypes], [spellType], [targetType], [requiredLevel], [trainingPrice], [manaCost], [soundFile]) VALUES (65, N'locate', N'Locate', N'0', N'castLocate', 1, N'6', 3, 4, 8, 0, 3, N'0226')

/****** Object:  Table [dbo].[SpawnZone]     ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SpawnZone](
	[zoneID] [int] IDENTITY(1,1) NOT NULL,
	[notes] [varchar](255) NULL,
	[enabled] [bit] NOT NULL,
	[npcID] [int] NOT NULL,
	[spawnTimer] [int] NOT NULL,
	[maxAllowedInZone] [int] NOT NULL,
	[spawnMessage] [varchar](255) NULL,
	[minZone] [int] NOT NULL,
	[maxZone] [int] NOT NULL,
	[npcList] [varchar](255) NULL,
	[spawnLand] [int] NOT NULL,
	[spawnMap] [int] NOT NULL,
	[spawnX] [int] NOT NULL,
	[spawnY] [int] NOT NULL,
	[spawnZ] [int] NOT NULL,
	[spawnRadius] [int] NOT NULL,
	[spawnZRange] [varchar](255) NULL,
 CONSTRAINT [PK_SpawnZone] PRIMARY KEY CLUSTERED 
(
	[zoneID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[SpawnZone] ON

INSERT [dbo].[SpawnZone] ([zoneID], [notes],     [enabled], [npcID], [spawnTimer], [maxAllowedInZone], [spawnMessage], [minZone], [maxZone], [npcList], [spawnLand], [spawnMap], [spawnX], [spawnY], [spawnZ], [spawnRadius], [spawnZRange]) 
   VALUES (               1,        N'Test Dog', 1,         920,     300,          1,                  NULL,           1,         1,         N'920',    0,           0,          44,       29,       0,        0,             NULL)

SET IDENTITY_INSERT [dbo].[SpawnZone] OFF
/****** Object:  Table [dbo].[Quest]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Quest](
	[questID] [int] IDENTITY(1,1) NOT NULL,
	[notes] [varchar](max) NULL,
	[name] [varchar](50) NULL,
	[description] [varchar](max) NULL,
	[completedDescription] [varchar](max) NULL,
	[requirements] [varchar](max) NULL,
	[requiredFlags] [varchar](max) NULL,
	[coinValues] [varchar](max) NULL,
	[rewardClass] [varchar](max) NULL,
	[rewardTitle] [varchar](max) NULL,
	[rewardFlags] [varchar](max) NULL,
	[requiredItems] [varchar](max) NULL,
	[rewardItems] [varchar](max) NULL,
	[rewardExperience] [varchar](max) NULL,
	[rewardStats] [varchar](max) NULL,
	[rewardTeleports] [varchar](max) NULL,
	[responseStrings] [varchar](max) NULL,
	[hintStrings] [varchar](max) NULL,
	[flagStrings] [varchar](max) NULL,
	[stepStrings] [varchar](max) NULL,
	[finishStrings] [varchar](max) NULL,
	[failStrings] [varchar](max) NULL,
	[classTypes] [nvarchar](max) NULL,
	[alignments] [nvarchar](max) NULL,
	[maximumLevel] [smallint] NOT NULL,
	[minimumLevel] [smallint] NOT NULL,
	[repeatable] [bit] NOT NULL,
	[stepOrder] [bit] NOT NULL,
	[soundFiles] [varchar](255) NULL,
	[totalSteps] [smallint] NOT NULL,
	[despawnsNPC] [bit] NOT NULL,
	[masterQuestID] [int] NOT NULL,
	[teleportGroup] [bit] NOT NULL,
 CONSTRAINT [PK_Quest] PRIMARY KEY CLUSTERED 
(
	[questID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Quest] ON

SET IDENTITY_INSERT [dbo].[Quest] OFF
/****** Object:  UserDefinedFunction [dbo].[fnCleanDefaultValue]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnCleanDefaultValue](@sDefaultValue varchar(4000))
RETURNS varchar(4000)
AS
BEGIN
	RETURN SubString(@sDefaultValue, 2, DataLength(@sDefaultValue)-2)
END
GO
/****** Object:  Table [dbo].[Facet]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Facet](
	[facetID] [smallint] IDENTITY(0,1) NOT NULL,
	[name] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Facet] ON
INSERT [dbo].[Facet] ([facetID], [name]) VALUES (0, N'Agate')
SET IDENTITY_INSERT [dbo].[Facet] OFF
/****** Object:  Table [dbo].[EffectTypeCode]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EffectTypeCode](
	[EffectTypeCode] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](255) NULL,
 CONSTRAINT [PK_EffectTypeCode] PRIMARY KEY CLUSTERED 
(
	[EffectTypeCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CharGen]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CharGen](
	[race] [varchar](20) NOT NULL,
	[class] [varchar](20) NOT NULL,
	[classType] [varchar](50) NOT NULL,
	[alignment] [int] NOT NULL,
	[bow] [int] NOT NULL,
	[dagger] [int] NOT NULL,
	[flail] [int] NOT NULL,
	[halberd] [int] NOT NULL,
	[mace] [int] NOT NULL,
	[rapier] [int] NOT NULL,
	[shuriken] [int] NOT NULL,
	[staff] [int] NOT NULL,
	[sword] [int] NOT NULL,
	[threestaff] [int] NOT NULL,
	[twoHanded] [int] NOT NULL,
	[thievery] [int] NOT NULL,
	[magic] [int] NOT NULL,
	[unarmed] [int] NOT NULL,
	[bash] [int] NOT NULL,
	[rightHand] [int] NOT NULL,
	[leftHand] [int] NOT NULL,
	[wearing] [varchar](50) NOT NULL,
	[sack] [varchar](50) NOT NULL,
	[belt] [varchar](50) NOT NULL,
	[spells] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

INSERT [dbo].[CharGen] ([race], [class], [classType], [alignment], [bow], [dagger], [flail], [halberd], [mace], [rapier], [shuriken], [staff], [sword], [threestaff], [twoHanded], [thievery], [magic], [unarmed], [bash], [rightHand], [leftHand], [wearing], [sack], [belt], [spells]) VALUES (N'Barbarian', N'Fighter', N'1', 1, 31, 31, 0, 0, 0, 0, 0, 4800, 2400, 0, 1500, 0, 0, 0, 4800, 24550, 100, N'8010 15010', N'0', N'25020', N'0')

/****** Object:  StoredProcedure [dbo].[prApp_CellDescription_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record in CellDescription
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_CellDescription_Update]
	@CellDescriptionID int,
	@LandID int,
	@MapID int,
	@Xcord int,
	@Ycord int,
	@CellDesc nvarchar(510) = NULL
AS

UPDATE	CellDescription
SET	LandID = @LandID,
	MapID = @MapID,
	Xcord = @Xcord,
	Ycord = @Ycord,
	CellDesc = @CellDesc
WHERE 	CellDescriptionID = @CellDescriptionID
GO
/****** Object:  StoredProcedure [dbo].[prApp_CellDescription_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Select a single record from CellDescription
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_CellDescription_Select]
	@CellDescriptionID int
AS

SELECT	CellDescriptionID,
	LandID,
	MapID,
	Xcord,
	Ycord,
	CellDesc
FROM	CellDescription
WHERE 	CellDescriptionID = @CellDescriptionID
GO
/****** Object:  StoredProcedure [dbo].[prApp_CellDescription_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into CellDescription
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_CellDescription_Insert]
	@LandID int,
	@MapID int,
	@Xcord int,
	@Ycord int,
	@CellDesc nvarchar(510) = NULL
AS

INSERT CellDescription(LandID, MapID, Xcord, Ycord, CellDesc)
VALUES (@LandID, @MapID, @Xcord, @Ycord, @CellDesc)

RETURN SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[prApp_CellDescription_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Delete a single record from CellDescription
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_CellDescription_Delete]
	@CellDescriptionID int
AS

DELETE	CellDescription
WHERE 	CellDescriptionID = @CellDescriptionID
GO
/****** Object:  Table [dbo].[Cell]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cell](
	[cellID] [int] IDENTITY(1,1) NOT NULL,
	[notes] [varchar](255) NULL,
	[mapName] [varchar](50) NOT NULL,
	[xCord] [int] NOT NULL,
	[yCord] [int] NOT NULL,
	[zCord] [int] NOT NULL,
	[segue] [varchar](50) NULL,
	[description] [nvarchar](510) NULL,
	[lock] [nvarchar](510) NULL,
	[portal] [bit] NOT NULL,
	[pvpEnabled] [bit] NOT NULL,
	[singleCustomer] [bit] NOT NULL,
	[teleport] [bit] NOT NULL,
	[mailbox] [bit] NOT NULL,
 CONSTRAINT [PK_Cell] PRIMARY KEY CLUSTERED 
(
	[cellID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Cell] ON
INSERT [dbo].[Cell] ([cellID], [notes], [mapName], [xCord], [yCord], [zCord], [segue], [description], [lock], [portal], [pvpEnabled], [singleCustomer], [teleport], [mailbox]) VALUES (1, NULL, N'test01', 41, 26, 0, NULL, N'You are standing in the northwest corner of test01.', NULL, 0, 0, 0, 0, 0)
SET IDENTITY_INSERT [dbo].[Cell] OFF
/****** Object:  Table [dbo].[CatalogItem]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CatalogItem](
	[catalogID] [int] IDENTITY(1,1) NOT NULL,
	[notes] [nvarchar](255) NULL,
	[combatAdds] [int] NOT NULL,
	[itemID] [int] NOT NULL,
	[itemType] [varchar](50) NOT NULL,
	[baseType] [varchar](50) NULL,
	[name] [nvarchar](50) NULL,
	[visualKey] [varchar](50) NOT NULL,
	[unidentifiedName] [nvarchar](150) NULL,
	[identifiedName] [nvarchar](150) NULL,
	[shortDesc] [nvarchar](255) NULL,
	[longDesc] [nvarchar](255) NULL,
	[wearLocation] [varchar](50) NOT NULL,
	[weight] [float] NOT NULL,
	[coinValue] [int] NULL,
	[size] [varchar](50) NULL,
	[effectType] [varchar](255) NULL,
	[effectAmount] [varchar](255) NULL,
	[effectDuration] [varchar](255) NULL,
	[special] [varchar](255) NULL,
	[minDamage] [int] NULL,
	[maxDamage] [int] NULL,
	[skillType] [varchar](50) NOT NULL,
	[vRandLow] [int] NULL,
	[vRandHigh] [int] NULL,
	[key] [varchar](50) NULL,
	[recall] [bit] NOT NULL,
	[alignment] [varchar](50) NOT NULL,
	[spell] [int] NOT NULL,
	[spellPower] [int] NOT NULL,
	[charges] [int] NOT NULL,
	[attackType] [varchar](50) NULL,
	[blueglow] [bit] NOT NULL,
	[flammable] [bit] NOT NULL,
	[fragile] [bit] NOT NULL,
	[lightning] [bit] NOT NULL,
	[returning] [bit] NULL,
	[silver] [bit] NOT NULL,
	[attuneType] [varchar](50) NOT NULL,
	[figExp] [int] NULL,
	[armorClass] [float] NOT NULL,
	[armorType] [int] NOT NULL,
	[bookType] [varchar](50) NOT NULL,
	[maxPages] [int] NOT NULL,
	[pages] [text] NOT NULL,
	[drinkDesc] [varchar](255) NOT NULL,
	[fluidDesc] [varchar](255) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[CatalogItem] ON

INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (239, N'Small Rock', 0, 30480, N'Miscellaneous', N'Gem', N'rock', N'unknown', N'Small Rock', N'Small Rock', N'a rock', N'a small rock', N'None', 0.1, 0, N'Sack_Only', N'', N'', N'0', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
/* Berries for the janitor to place */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (288, N'Bright Red Berries (+6 heal) *ALL*', 0, 33020, N'Edible', N'Food', N'berries', N'unknown', N'Red Berries', N'Red Berries', N'a bunch of berries', N'a bundle of bright red berries', N'None', 0, 0, N'Sack_Only', N'', N'', N'0', N'balmberry', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (289, N'Yellow Berries (poison) *ALL*', 0, 33021, N'Edible', N'Food', N'berries', N'unknown', N'Yellow Berries', N'Yellow Berries', N'a bunch of berries', N'a bundle of yellow berries', N'None', 0, 0, N'Sack_Only', N'', N'', N'0', N'poisonberry', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (290, N'Shiny Blue Berries (+6 mana)', 0, 33022, N'Edible', N'Food', N'berries', N'unknown', N'Blue Berries', N'Blue Berries', N'a bunch of berries', N'a bundle of shiny blue berries', N'None', 0, 0, N'Sack_Only', N'', N'', N'0', N'manaberry', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (291, N'Dark Green Berries (+6 stamina)', 0, 33023, N'Edible', N'Food', N'berries', N'unknown', N'Green Berries', N'Green Berries', N'a bunch of berries', N'a bundle of dark green berries', N'None', 0, 0, N'Sack_Only', N'', N'', N'0', N'stamberry', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
/* Fur for the dog */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (363, N'Dog Fur', 0, 7206, N'Wearable', N'Armor', N'fur', N'unknown', N'Dog Skin Jacket', N'Dog Skin Jacket', N'a jacket', N'a small jacket made from the skin of a dog', N'Torso', 0.1, 0, N'Belt_Or_Sack', N'', N'', N'0', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 2, N'None', 0, N'', N'', N'')
/* Fighter Barbarian supplies */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (259, N'Wooden Shield', 0, 100, N'Miscellaneous', N'Shield', N'shield', N'unknown', N'Wooden Shield', N'Wooden Shield', N'a shield', N'a wooden shield', N'Back', 4, 15, N'Belt_Only', N'36', N'1', N'0', N'', 1, 4, N'Bash', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 1.2, 0, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (30, N'Leather Tunic', 0, 8010, N'Wearable', N'Armor', N'tunic', N'unknown', N'Leather Tunic', N'Leather Tunic', N'a leather tunic', N'a leather tunic', N'Torso', 5, 200, N'Belt_Large_Slot_Only', N'', N'', N'0', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0.8, 5, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (149, N'Wooden Spear', 0, 24550, N'Weapon', N'Staff', N'spear', N'unknown', N'Wooden Spear', N'Wooden Spear', N'a spear', N'a wooden spear with an iron tip', N'None', 3, 5, N'Belt_Only', N'', N'', N'0', N'pierce', 1, 6, N'Staff', 0, 0, N'', 0, N'None', -1, -1, -1, N'spear', 0, 0, 0, 0, 0, 0, N'None', 0, 1, 0, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (277, N'Leather Leggings', 0, 15010, N'Wearable', N'Armor', N'leggings', N'unknown', N'Leather Leggings', N'Leather Leggings', N'leather leggings', N'a pair of leather leggings', N'Legs', 3.1, 25, N'Belt_Only', N'', N'', N'0', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 1.1, 5, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (162, N'Iron Shortsword', 0, 25020, N'Weapon', N'Sword', N'shortsword', N'unknown', N'Iron Shortsword', N'Iron Shortsword', N'a shortsword', N'an iron shortsword', N'None', 3, 2, N'Belt_Only', N'', N'', N'0', N'slash', 1, 6, N'Sword', 0, 0, N'', 0, N'None', -1, -1, -1, N'shortsword', 0, 0, 0, 0, 0, 0, N'None', 0, 0.8, 0, N'None', 0, N'', N'', N'')
/* corpse */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (300, N'Generic Corpse', 0, 30001, N'Corpse', N'Unknown', N'corpse', N'unknown', N'Corpse', N'Corpse', N'a corpse', N'a corpse', N'None', 10, 0, N'No_Container', N'', N'', N'', N'', 1, 1, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 1, 0, 0, 0, 0, N'None', 0, -5, 0, N'None', 0, N' ', N' ', N' ')
/* supplies for new characters */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (196, N'Gold Coins', 0, 30000, N'Coin', N'Unknown', N'coins', N'unknown', N'Coins', N'Coins', N'coins', N'A pile of gold coins', N'None', 1, 1, N'Sack_Only', N'', N'', N'0', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (242, N'Spellbook', 0, 31000, N'Miscellaneous', N'Book', N'spellbook', N'unknown', N'Spellbook', N'Spellbook', N'a spellbook', N'a small leather book with mystic runes engraved into cover', N'None', 1.3, 0, N'Sack_Only', N'', N'', N'0', N'', 1, 4, N'Magic', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'Take', 0, -5, 0, N'Spellbook', 50, N'', N'', N'')
/* Recall ring and small gold ring*/
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) VALUES (57, N'Small Gold Ring / Blue Glow (recall)', 0, 13990, N'Wearable', N'Ring', N'ring', N'unknown', N'Recall Ring', N'Recall Ring', N'a ring', N'a small gold ring', N'Finger', 0, 150, N'Sack_Only', N'', N'', N'0', N'blueglow', 1, 4, N'None', 0, 0, N'', 1, N'None', -1, -1, -1, N'', 1, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) 
   VALUES (69, N'Small Gold Ring', 0, 13100, N'Wearable', N'Ring', N'ring', N'unknown', N'Gold Ring', N'Gold Ring', N'a ring', N'a small gold ring', N'Finger', 0, 20, N'Sack_Only', N'', N'', N'0', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')
/* wolf fur */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc]) 
   VALUES (270, N'Wolf Fur *BG*', 0, 7201, N'Wearable', N'Armor', N'jacket', N'unknown', N'Wolf Fur Jacket', N'Wolf Fur Jacket', N'a jacket', N'a jacket made from the fur of a wolf', N'Back', 1.1, 25, N'Belt_Or_Sack', N'', N'0', N'0', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 0, 0, 0, 0, N'None', 0, 0.2, 2, N'None', 0, N'', N'', N'')
/* stone halberd (very heavy) */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (130, N'Stone Halberd', 0, 22550, N'Weapon', N'Halberd', N'halberd', N'unknown', N'Stone Halberd', N'Stone Halberd', N'a halberd', N'a stone halberd', N'Back', 112.1, 1, N'No_Container', N'', N'', N'0', N'blunt', 2, 24, N'Halberd', 0, 0, N'', 0, N'None', -1, -1, -1, N'halberd', 0, 0, 0, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'')

/* potions */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (87, N'Balm (260 hits)', 0, 20000, N'Potable', N'Bottle', N'balm', N'unknown', N'Green Glass Bottle', N'Green Glass Bottle', N'a balm', N'a green glass bottle', N'None', 0.1, 16, N'Sack_Only', N'57', N'260', N'10', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'a cloudy white liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (88, N'Stamina Restore Potion *AG/Axe*', 0, 20010, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a pale blue bottle streaked with white lines', N'None', 0.2, 31, N'Sack_Only', N'68', N'500', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'It tastes like apple juice.', N'a clear, jade coloured liquid that smells of jasmine.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (89, N'Mana Restore Potion *AG*', 0, 20020, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a white porcelain bottle decorated with red lotus blossums', N'None', 0.2, 51, N'Sack_Only', N'67', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'It tastes sweet.', N'an amber liquid that smells faintly of orange blossoms.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (90, N'Permanent Charisma Potion *Rift/Oak*', 0, 20030, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a porcelain bottle with small irises in a raised relief around it''s base', N'None', 0.2, 1000, N'Sack_Only', N'29', N'1', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You feel pretty. Oh so pretty.', N'a creamy white liquid with the strong odour of vanilla.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (91, N'Drake Potion', 0, 20040, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a small porcelain vial', N'None', 0.2, 2000, N'Sack_Only', N'65', N'4', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You feel invigorated!', N'a deep blue liquid, with a slight head of lavender foam.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (92, N'Permanent Dexterity Potion *ALL*', 0, 20050, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a black ceramic bottle with gold whorls around the base', N'None', 0.2, 1200, N'Sack_Only', N'25', N'1', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'Your entire body goes numb as your vision blurs!', N'a glistening silver fluid with the faint odour of mint.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (93, N'Permanent Strength Potion *ALL*', 0, 20060, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a reddish clay bottle with three concentric cirlces painted on one side', N'None', 0.2, 3000, N'Sack_Only', N'24', N'1', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'Your heart races, and your muscles suddenly surge with power.', N'a dark crimson liquid that smells of earth and mushrooms.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (94, N'Temporary Strength Potion *ALL*', 0, 20070, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a red bottle', N'None', 0.2, 26, N'Sack_Only', N'30', N'3', N'180', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You have been enchanted with Strength!', N'a clear red liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (95, N'Ale', 0, 20080, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a brown glass bottle', N'None', 0.2, 2, N'Sack_Only', N'59', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'BURP!', N'a golden brown liquid, with a slight head of foam.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (96, N'Dr. Taylors', 0, 20090, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a clear glass bottle labeled ''Dr. Taylor''s Snake Oil and Hair Tonic. Cures all ailments. 130 proof''', N'None', 0.2, 5, N'Sack_Only', N'66', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'Mmminty.', N'a brown liquid that has a minty odour.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (97, N'Coffee', 0, 20100, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a green glass bottle', N'None', 0.2, 3, N'Sack_Only', N'62', N'1', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You feel refreshed.', N'a dark brown liquid with the aroma of roasted coffee beans.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (98, N'Drake Potion (mother of pearl)', 0, 20110, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a mother-of-pearl bottle, etched with delicate tracery', N'None', 0.2, 20, N'Sack_Only', N'65', N'4', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You feel invigorated!', N'a deep blue liquid, with a slight head of lavender foam.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (99, N'Empty Bottle', 0, 20120, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a green glass bottle', N'None', 0.1, 1, N'Sack_Only', N'0', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You feel refreshed.', N'a clear liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (100, N'Wine', 0, 20130, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a flat bottomed bottle', N'None', 0.1, 15, N'Sack_Only', N'60', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'BURP!', N'a clear dark purple liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (101, N'Naphtha', 0, 20140, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a black ceramic bottle', N'None', 0.2, 26, N'Sack_Only', N'58', N'15', N'10', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'Ugh. It tastes like liquid fire.', N'a clear liquid with a powerful odour.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (102, N'Nitro', 0, 20150, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a green glass bottle', N'None', 0.2, 1, N'Sack_Only', N'69', N'15', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'That probably wasn''t a good idea...', N'a clear liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (103, N'Orc Balm', 0, 20160, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a green glass bottle', N'None', 0.2, 20, N'Sack_Only', N'70', N'6', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You are stunned!', N'an intensely yellow, almost orange liquid with an overpowering stench.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (104, N'Permanent Intelligence Potion *AG/Axe*', 0, 20170, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a porcelain bottle stamped with a gold sun', N'None', 0.2, 1101, N'Sack_Only', N'26', N'1', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You begin to think more clearly.', N'a golden wine perfumed with the scent of wildflowers.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (105, N'Poison', 0, 20180, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a green glass bottle', N'None', 0.2, 11, N'Sack_Only', N'56', N'25', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'a clear liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (106, N'Poison, Death *Axe*', 0, 20190, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a heavy gold bottle bearing somber funeral scenes', N'None', 0.7, 0, N'Sack_Only', N'56', N'1000', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'a clear liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (107, N'Stamina Restore Potion *Oak*', 0, 20200, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a burnished steel bottle with the icon of a falcon', N'None', 0.2, 35, N'Sack_Only', N'68', N'20', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'It tastes like apple juice.', N'a clear, jade coloured liquid that smells of jasmine.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (108, N'Temporary Strength Potion *Oak*', 0, 20210, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a steel bottle stamped with the icon of a bull', N'None', 0.2, 30, N'Sack_Only', N'30', N'5', N'260', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You have been enchanted with Strength!', N'a clear red liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (109, N'Water', 0, 20220, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a green glass bottle', N'None', 0.1, 5, N'Sack_Only', N'63', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You feel refreshed.', N'a clear liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (110, N'Youth Potion', 0, 20230, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a silver bottle decorated with shiny rubellite gemstones', N'None', 0.1, 1000, N'Sack_Only', N'64', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'', N'a bluish liquid with tiny bubbles.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (111, N'Mug of Ale', 0, 20240, N'Potable', N'Bottle', N'mug', N'unknown', N'Pewter Mug', N'Pewter Mug', N'a mug', N'a pewter mug', N'None', 0.5, 10, N'Sack_Only', N'59', N'10', N'45', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'BURP!', N'a golden brown liquid, with a slight head of foam.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (112, N'Whiskey', 0, 20250, N'Potable', N'Bottle', N'bottle', N'unknown', N'Whiskey', N'Whiskey', N'a bottle', N'a bottle of whiskey', N'None', 0.2, 12, N'Sack_Only', N'72', N'20', N'90', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'BURP!', N'a clear liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (113, N'Lemurian Wine', 0, 20260, N'Potable', N'Bottle', N'flask', N'unknown', N'Lemurian Wine', N'Lemurian Wine', N'a flask', N'a blue flask of Lemurian wine', N'None', 0.1, 3, N'Sack_Only', N'60', N'30', N'180', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'BURP!', N'a clear dark purple liquid.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (114, N'Rum', 0, 20270, N'Potable', N'Bottle', N'bottle', N'unknown', N'Rum', N'Rum', N'a bottle', N'a bottle of rum', N'None', 0.2, 14, N'Sack_Only', N'73', N'40', N'220', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'BURP!', N'a clear liquid with a powerful odour.')
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (307, N'Drake Potion (Con Only)', 0, 20041, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a large porcelain vial', N'None', 1, 1000, N'Sack_Only', N'28', N'2', N'1', N'', 1, 2, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You feel invigorated with renewed health!', N'a deep blue liquid, with a slight head of lavender foam.')

/*** Begin test items at CatalogItem 500, itemID 50000 */
/* Generic potion */
INSERT [dbo].[CatalogItem] ([catalogID], [notes], [combatAdds], [itemID], [itemType], [baseType], [name], [visualKey], [unidentifiedName], [identifiedName], [shortDesc], [longDesc], [wearLocation], [weight], [coinValue], [size], [effectType], [effectAmount], [effectDuration], [special], [minDamage], [maxDamage], [skillType], [vRandLow], [vRandHigh], [key], [recall], [alignment], [spell], [spellPower], [charges], [attackType], [blueglow], [flammable], [fragile], [lightning], [returning], [silver], [attuneType], [figExp], [armorClass], [armorType], [bookType], [maxPages], [pages], [drinkDesc], [fluidDesc])
   VALUES (500, N'Generic Potion', 0, 50000, N'Potable', N'Bottle', N'bottle', N'unknown', N'Bottle', N'Bottle', N'a bottle', N'a generic bottle', N'None', 0.2, 20, N'Sack_Only', N'', N'0', N'1', N'', 1, 4, N'None', 0, 0, N'', 0, N'None', -1, -1, -1, N'', 0, 0, 1, 0, 0, 0, N'None', 0, 0, 0, N'None', 0, N'', N'You drank the potion!', N'a generic liquid.')

SET IDENTITY_INSERT [dbo].[CatalogItem] OFF
/****** Object:  Table [dbo].[BannedIP]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BannedIP](
	[bannedIP] [varchar](50) NULL,
	[dateBanned] [datetime] NULL,
	[reason] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Account]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[accountID] [int] IDENTITY(1,1) NOT NULL,
	[notes] [text] NULL,
	[account] [nvarchar](20) NULL,
	[password] [nvarchar](20) NULL,
	[IPAddress] [nvarchar](16) NULL,
	[IPAddressList] [nvarchar](1160) NULL,
	[currentMarks] [int] NOT NULL,
	[lifetimeMarks] [int] NOT NULL,
	[lastOnline] [datetime] NOT NULL,
	[banLength] [int] NOT NULL,
	[banDate] [datetime] NULL,
	[email] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK__Account__03A67F89] PRIMARY KEY NONCLUSTERED 
(
	[accountID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON
INSERT [dbo].[Account] ([accountID], [notes], [account], [password], [IPAddress], [IPAddressList], [currentMarks], [lifetimeMarks], [lastOnline], [banLength], [banDate], [email]) VALUES (1, NULL, N'test123', N'test123', N'::1', N'fe80::b96d:e7b:3046:34b%11,::1', 0, 0, CAST(0x0000A550005C4900 AS DateTime), 0, NULL, N'test123@test.not')
SET IDENTITY_INSERT [dbo].[Account] OFF
/****** Object:  Table [dbo].[NPC]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NPC](
	[catalogID] [int] IDENTITY(1,1) NOT NULL,
	[npcID] [int] NOT NULL,
	[notes] [text] NULL,
	[name] [nvarchar](255) NOT NULL,
	[attackSound] [varchar](50) NULL,
	[deathSound] [varchar](50) NULL,
	[idleSound] [varchar](50) NULL,
	[movementString] [nvarchar](255) NULL,
	[shortDesc] [varchar](50) NULL,
	[longDesc] [varchar](max) NULL,
	[visualKey] [varchar](50) NULL,
	[classFullName] [varchar](50) NULL,
	[baseArmorClass] [float] NOT NULL,
	[thac0Adjustment] [int] NOT NULL,
	[special] [nvarchar](255) NULL,
	[npcType] [varchar](50) NOT NULL,
	[aiType] [varchar](50) NOT NULL,
	[species] [varchar](50) NOT NULL,
	[gold] [int] NULL,
	[classType] [varchar](50) NOT NULL,
	[exp] [int] NULL,
	[hitsMax] [int] NULL,
	[alignment] [varchar](50) NOT NULL,
	[stamina] [int] NOT NULL,
	[manaMax] [int] NOT NULL,
	[speed] [int] NOT NULL,
	[strength] [int] NOT NULL,
	[dexterity] [int] NOT NULL,
	[intelligence] [int] NOT NULL,
	[wisdom] [int] NOT NULL,
	[constitution] [int] NOT NULL,
	[charisma] [int] NOT NULL,
	[lootVeryCommonAmount] [int] NOT NULL,
	[lootVeryCommonArray] [varchar](1000) NULL,
	[lootVeryCommonOdds] [int] NOT NULL,
	[lootCommonAmount] [int] NOT NULL,
	[lootCommonArray] [varchar](1000) NOT NULL,
	[lootCommonOdds] [int] NOT NULL,
	[lootRareAmount] [int] NOT NULL,
	[lootRareArray] [varchar](1000) NULL,
	[lootRareOdds] [int] NOT NULL,
	[lootVeryRareAmount] [int] NOT NULL,
	[lootVeryRareArray] [varchar](1000) NULL,
	[lootVeryRareOdds] [int] NOT NULL,
	[lootLairAmount] [int] NOT NULL,
	[lootLairArray] [varchar](1000) NULL,
	[lootLairOdds] [int] NOT NULL,
	[lootAlwaysArray] [varchar](1000) NULL,
	[lootBeltAmount] [int] NULL,
	[lootBeltArray] [varchar](1000) NULL,
	[lootBeltOdds] [int] NOT NULL,
	[spawnArmorArray] [varchar](1000) NULL,
	[spawnLeftHandArray] [varchar](1000) NULL,
	[spawnLeftHandOdds] [int] NULL,
	[spawnRightHandArray] [varchar](1000) NULL,
	[mace] [bigint] NOT NULL,
	[bow] [bigint] NOT NULL,
	[dagger] [bigint] NOT NULL,
	[flail] [bigint] NOT NULL,
	[rapier] [bigint] NOT NULL,
	[twoHanded] [bigint] NOT NULL,
	[staff] [bigint] NOT NULL,
	[shuriken] [bigint] NOT NULL,
	[sword] [bigint] NOT NULL,
	[threestaff] [bigint] NOT NULL,
	[halberd] [bigint] NOT NULL,
	[thievery] [bigint] NOT NULL,
	[unarmed] [bigint] NOT NULL,
	[magic] [bigint] NOT NULL,
	[bash] [bigint] NOT NULL,
	[level] [smallint] NOT NULL,
	[animal] [bit] NOT NULL,
	[tanningResult] [varchar](255) NULL,
	[undead] [bit] NOT NULL,
	[spectral] [bit] NOT NULL,
	[hidden] [bit] NOT NULL,
	[poisonous] [int] NOT NULL,
	[waterDweller] [bit] NOT NULL,
	[fly] [bit] NOT NULL,
	[breatheWater] [bit] NOT NULL,
	[nightVision] [bit] NOT NULL,
	[lair] [bit] NOT NULL,
	[lairCells] [varchar](255) NULL,
	[mobile] [bit] NOT NULL,
	[command] [bit] NOT NULL,
	[castMode] [int] NOT NULL,
	[randomName] [bit] NOT NULL,
	[attackString1] [varchar](255) NULL,
	[attackString2] [varchar](255) NULL,
	[attackString3] [varchar](255) NULL,
	[attackString4] [varchar](255) NULL,
	[attackString5] [varchar](255) NULL,
	[attackString6] [varchar](255) NULL,
	[blockString1] [varchar](255) NULL,
	[blockString2] [varchar](255) NULL,
	[blockString3] [varchar](255) NULL,
	[merchantType] [varchar](50) NOT NULL,
	[merchantMarkup] [float] NOT NULL,
	[trainerType] [varchar](50) NOT NULL,
	[interactiveType] [varchar](50) NOT NULL,
	[gender] [int] NOT NULL,
	[race] [varchar](255) NOT NULL,
	[age] [int] NOT NULL,
	[patrol] [bit] NOT NULL,
	[patrolRoute] [varchar](1000) NULL,
	[immuneFire] [bit] NOT NULL,
	[immuneCold] [bit] NOT NULL,
	[immunePoison] [bit] NOT NULL,
	[immuneLightning] [bit] NOT NULL,
	[immuneCurse] [bit] NOT NULL,
	[immuneDeath] [bit] NOT NULL,
	[immuneStun] [bit] NOT NULL,
	[immuneFear] [bit] NOT NULL,
	[immuneBlind] [bit] NOT NULL,
	[spells] [varchar](max) NULL,
	[quests] [varchar](max) NULL,
	[groupAmount] [smallint] NOT NULL,
	[groupMembers] [varchar](1000) NULL,
	[weaponRequirement] [varchar](255) NULL,
	[questFlags] [varchar](255) NULL,
 CONSTRAINT [PK_NPC] PRIMARY KEY CLUSTERED 
(
	[catalogID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[NPC] ON

INSERT [dbo].[NPC] ([catalogID], [npcID], [notes], [name], [attackSound], [deathSound], [idleSound], [movementString], [shortDesc], [longDesc], [visualKey], [classFullName], [baseArmorClass], [thac0Adjustment], [special], [npcType], [aiType], [species], [gold], [classType], [exp], [hitsMax], [alignment], [stamina], [manaMax], [speed], [strength], [dexterity], [intelligence], [wisdom], [constitution], [charisma], [lootVeryCommonAmount], [lootVeryCommonArray], [lootVeryCommonOdds], [lootCommonAmount], [lootCommonArray], [lootCommonOdds], [lootRareAmount], [lootRareArray], [lootRareOdds], [lootVeryRareAmount], [lootVeryRareArray], [lootVeryRareOdds], [lootLairAmount], [lootLairArray], [lootLairOdds], [lootAlwaysArray], [lootBeltAmount], [lootBeltArray], [lootBeltOdds], [spawnArmorArray], [spawnLeftHandArray], [spawnLeftHandOdds], [spawnRightHandArray], [mace], [bow], [dagger], [flail], [rapier], [twoHanded], [staff], [shuriken], [sword], [threestaff], [halberd], [thievery], [unarmed], [magic], [bash], [level], [animal], [tanningResult], [undead], [spectral], [hidden], [poisonous], [waterDweller], [fly], [breatheWater], [nightVision], [lair], [lairCells], [mobile], [command], [castMode], [randomName], [attackString1], [attackString2], [attackString3], [attackString4], [attackString5], [attackString6], [blockString1], [blockString2], [blockString3], [merchantType], [merchantMarkup], [trainerType], [interactiveType], [gender], [race], [age], [patrol], [patrolRoute], [immuneFire], [immuneCold], [immunePoison], [immuneLightning], [immuneCurse], [immuneDeath], [immuneStun], [immuneFear], [immuneBlind], [spells], [quests], [groupAmount], [groupMembers], [weaponRequirement], [questFlags]) 
   VALUES (73, 920, N'Kesmai Dog', N'dog', NULL, NULL, NULL, N'a dog barking, ''Woof woof woof!''.', N'dog', N'a dog', N'dog', N'Fighter', 10, 0, N'', N'Creature', N'Unknown', N'DomesticAnimal', 0, N'Fighter', 0, 0, N'Lawful', 0, 0, 3, 8, 11, 3, 3, 10, 10, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, N'', 0, N'', 0, N'7206', N'0', 0, N'', 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 0, 1600, 0, 1000, 1, 1, N'7206', 0, 0, 0, 0, 0, 0, 0, 1, 0, N'', 0, 0, 0, 0, N'The dog bites you!', N'The dog bites you!', N'The dog savages you with it''s teeth!', N'The dog bites you!', N'The dog savages you with it''s teeth!', N'The dog bites you!', N'You are blocked by the dog''s paw.', N'The dog dodges your attack.', N'The dog dodges your attack.', N'None', 0, N'None', N'None', 0, N'', 0, 1, N'42|31|0 44|32|0 46|32|0 48|32|0 49|30|0 49|28|0 50|26|0 48|24|0 48|22|0 48|20|0 50|18|0 51|18|0 49|19|0 48|21|0 48|23|0 46|24|0 44|24|0 42|25|0 41|26|0 41|29|0', 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, NULL, NULL, NULL)
INSERT [dbo].[NPC] ([catalogID], [npcID], [notes], [name], [attackSound], [deathSound], [idleSound], [movementString], [shortDesc], [longDesc], [visualKey], [classFullName], [baseArmorClass], [thac0Adjustment], [special], [npcType], [aiType], [species], [gold], [classType], [exp], [hitsMax], [alignment], [stamina], [manaMax], [speed], [strength], [dexterity], [intelligence], [wisdom], [constitution], [charisma], [lootVeryCommonAmount], [lootVeryCommonArray], [lootVeryCommonOdds], [lootCommonAmount], [lootCommonArray], [lootCommonOdds], [lootRareAmount], [lootRareArray], [lootRareOdds], [lootVeryRareAmount], [lootVeryRareArray], [lootVeryRareOdds], [lootLairAmount], [lootLairArray], [lootLairOdds], [lootAlwaysArray], [lootBeltAmount], [lootBeltArray], [lootBeltOdds], [spawnArmorArray], [spawnLeftHandArray], [spawnLeftHandOdds], [spawnRightHandArray], [mace], [bow], [dagger], [flail], [rapier], [twoHanded], [staff], [shuriken], [sword], [threestaff], [halberd], [thievery], [unarmed], [magic], [bash], [level], [animal], [tanningResult], [undead], [spectral], [hidden], [poisonous], [waterDweller], [fly], [breatheWater], [nightVision], [lair], [lairCells], [mobile], [command], [castMode], [randomName], [attackString1], [attackString2], [attackString3], [attackString4], [attackString5], [attackString6], [blockString1], [blockString2], [blockString3], [merchantType], [merchantMarkup], [trainerType], [interactiveType], [gender], [race], [age], [patrol], [patrolRoute], [immuneFire], [immuneCold], [immunePoison], [immuneLightning], [immuneCurse], [immuneDeath], [immuneStun], [immuneFear], [immuneBlind], [spells], [quests], [groupAmount], [groupMembers], [weaponRequirement], [questFlags]) 
   VALUES (29, 11, N'Wolf (3)', N'wolf', N'0026', N'0038', N'0014', N'', N'gray wolf', N'a wild gray wolf', N'gray_wolf', N'Fighter', 10, 0, N'', N'Creature', N'Unknown', N'WildAnimal', 0, N'Fighter', 500, 0, N'Chaotic', 0, 0, 2, 14, 15, 3, 3, 12, 5, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, N'', 0, N'', 0, N'7201', N'', 0, N'', 6400, 6400, 6400, 6400, 6400, 6400, 6400, 6400, 6400, 6400, 6400, 0, 3200, 0, 0, 5, 1, N'7201', 0, 0, 0, 0, 0, 0, 0, 1, 0, N'', 1, 0, 0, 0, N'The wolf bites you!', N'The wolf slashes you with it''s claws!', N'The wolf bites you!', N'The wolf slashes you with it''s claws!', N'The wolf bites you!', N'The wolf slashes you with it''s claws!', N'You are blocked by the wolf''s paw.', N'You are blocked by the wolf''s paw.', N'You are blocked by the wolf''s paw.', N'None', 0, N'None', N'None', 0, N'', 0, 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 3, NULL, NULL, NULL)

SET IDENTITY_INSERT [dbo].[NPC] OFF
/****** Object:  Table [dbo].[Map]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Map](
	[mapID] [smallint] NOT NULL,
	[landID] [smallint] NOT NULL,
	[name] [varchar](50) NULL,
	[shortDesc] [varchar](max) NULL,
	[longDesc] [varchar](max) NULL,
	[suggestedMaximumLevel] [smallint] NOT NULL,
	[suggestedMinimumLevel] [smallint] NOT NULL,
	[pvpEnabled] [bit] NOT NULL,
	[expModifier] [float] NOT NULL,
	[difficulty] [smallint] NOT NULL,
	[climateType] [smallint] NOT NULL,
	[balmBushes] [bit] NOT NULL,
	[poisonBushes] [bit] NOT NULL,
	[manaBushes] [bit] NOT NULL,
	[staminaBushes] [bit] NOT NULL,
	[resX] [int] NOT NULL,
	[resY] [int] NOT NULL,
	[resZ] [int] NOT NULL,
	[thiefResX] [int] NOT NULL,
	[thiefResY] [int] NOT NULL,
	[thiefResZ] [int] NOT NULL,
	[karmaResX] [int] NOT NULL,
	[karmaResY] [int] NOT NULL,
	[karmaResZ] [int] NOT NULL,
	[randomMagicIntensity] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[Map] ([mapID], [landID], [name], [shortDesc], [longDesc], [suggestedMaximumLevel], [suggestedMinimumLevel], [pvpEnabled], [expModifier], [difficulty], [climateType], [balmBushes], [poisonBushes], [manaBushes], [staminaBushes], [resX], [resY], [resZ], [thiefResX], [thiefResY], [thiefResZ], [karmaResX], [karmaResY], [karmaResZ], [randomMagicIntensity]) 
   VALUES (0, 0, N'test01', N'test01', NULL, 12, 3, 0, 1, 1, 2, 1, 1, 0, 0, 41, 26, 0, 42, 27, 0, 43, 28, 0, 0)
INSERT [dbo].[Map] ([mapID], [landID], [name], [shortDesc], [longDesc], [suggestedMaximumLevel], [suggestedMinimumLevel], [pvpEnabled], [expModifier], [difficulty], [climateType], [balmBushes], [poisonBushes], [manaBushes], [staminaBushes], [resX], [resY], [resZ], [thiefResX], [thiefResY], [thiefResZ], [karmaResX], [karmaResY], [karmaResZ], [randomMagicIntensity]) 
   VALUES (4, 2, N'testunder', N'testunder', NULL, 12, 3, 0, 1, 1, 2, 1, 1, 0, 0, 41, 26, 0, 42, 27, 0, 43, 28, 0, 0)

/****** Object:  Table [dbo].[MailAttachment]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MailAttachment](
	[mailID] [bigint] NOT NULL,
	[itemID] [int] NOT NULL,
	[attunedID] [int] NOT NULL,
	[special] [varchar](255) NOT NULL,
	[coinValue] [float] NOT NULL,
	[charges] [int] NOT NULL,
	[attuneType] [varchar](50) NOT NULL,
	[figExp] [bigint] NOT NULL,
	[timeCreated] [datetime] NOT NULL,
	[whoCreated] [nvarchar](100) NOT NULL,
	[paymentRequested] [float] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Mail]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Mail](
	[mailID] [bigint] IDENTITY(1,1) NOT NULL,
	[senderID] [int] NOT NULL,
	[receiverID] [int] NOT NULL,
	[timeSent] [datetime] NOT NULL,
	[subject] [nvarchar](50) NOT NULL,
	[body] [varchar](max) NOT NULL,
	[attachment] [bit] NOT NULL,
	[readByReceiver] [bit] NOT NULL,
 CONSTRAINT [PK_Mail] PRIMARY KEY CLUSTERED 
(
	[mailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[LivePC]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LivePC](
	[uniqueId] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[facet] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
	[level] [int] NOT NULL,
	[hits] [int] NOT NULL,
	[fullHits] [int] NOT NULL,
	[mana] [int] NOT NULL,
	[fullMana] [int] NOT NULL,
	[lastCommand] [nvarchar](255) NOT NULL,
	[lastActiveRound] [int] NOT NULL,
	[firstActiveRound] [int] NOT NULL,
	[isDead] [bit] NOT NULL,
	[effects] [nvarchar](255) NULL,
 CONSTRAINT [PK_LivePC] PRIMARY KEY CLUSTERED 
(
	[uniqueId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LiveNPC]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LiveNPC](
	[uniqueId] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[facet] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
	[level] [int] NOT NULL,
	[hits] [int] NOT NULL,
	[fullHits] [int] NOT NULL,
	[mana] [int] NOT NULL,
	[fullMana] [int] NOT NULL,
	[lastCommand] [nvarchar](255) NOT NULL,
	[lastActiveRound] [int] NOT NULL,
	[firstActiveRound] [int] NOT NULL,
	[isDead] [bit] NOT NULL,
	[mostHatedId] [int] NULL,
	[hateCenterX] [smallint] NULL,
	[hateCenterY] [smallint] NULL,
	[loveCenterX] [smallint] NULL,
	[loveCenterY] [smallint] NULL,
	[fearLove] [int] NULL,
	[NpcTypeCode] [int] NULL,
	[BaseTypeCode] [int] NULL,
	[CharacterClassCode] [int] NULL,
	[AlignCode] [int] NULL,
	[numAttackers] [int] NULL,
	[lairLocationX] [smallint] NULL,
	[lairLocationY] [smallint] NULL,
	[lairLocationZ] [int] NULL,
	[priorityCode] [int] NULL,
	[effects] [nvarchar](255) NULL,
 CONSTRAINT [PK_LiveNPC] PRIMARY KEY CLUSTERED 
(
	[uniqueId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LiveCell]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[LiveCell](
	[lastRoundChanged] [int] NOT NULL,
	[facet] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
	[cellGraphic] [char](2) NOT NULL,
	[displayGraphic] [char](2) NOT NULL,
 CONSTRAINT [PK_LiveCell] PRIMARY KEY CLUSTERED 
(
	[facet] ASC,
	[map] ASC,
	[xCord] ASC,
	[yCord] ASC,
	[zCord] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[Land]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Land](
	[landID] [smallint] IDENTITY(0,1) NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[shortDesc] [nvarchar](255) NULL,
	[longDesc] [nvarchar](4000) NULL,
	[hitDice] [nvarchar](255) NULL,
	[manaDice] [nvarchar](255) NULL,
	[staminaDice] [nvarchar](255) NULL,
	[statCapOperand] [int] NULL,
	[maximumAbilityScore] [int] NULL,
	[maximumShielding] [int] NULL,
	[maximumTempAbilityScore] [int] NULL,
 CONSTRAINT [PK_Land] PRIMARY KEY CLUSTERED 
(
	[landID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Land] ON

INSERT [dbo].[Land] ([landID], [name], [shortDesc], [longDesc], [hitDice], [manaDice], [staminaDice], [statCapOperand], [maximumAbilityScore], [maximumShielding], [maximumTempAbilityScore]) 
   VALUES (0, N'Beginner''s Game', N'BG', N'the Beginner''s Game', N'0 12 8 6 10 8 10', N'0 0 6 8 0 4 0', N'0 6 4 4 8 4 4', 32, 18, 9, 12)
INSERT [dbo].[Land] ([landID], [name], [shortDesc], [longDesc], [hitDice], [manaDice], [staminaDice], [statCapOperand], [maximumAbilityScore], [maximumShielding], [maximumTempAbilityScore]) 
   VALUES (2, N'Underworld', N'UW', N'the Underworld', N'0 12 8 6 10 8 10', N'0 0 6 8 0 4 0', N'0 6 4 4 8 4 4', 32, 18, 9, 18)

SET IDENTITY_INSERT [dbo].[Land] OFF
/****** Object:  Table [dbo].[PlayerWearing]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerWearing](
	[playerID] [int] NOT NULL,
	[wearingSlot] [int] NULL,
	[itemID] [int] NULL,
	[attunedID] [int] NOT NULL,
	[wearOrientation] [int] NOT NULL,
	[special] [nvarchar](1000) NOT NULL,
	[coinvalue] [float] NOT NULL,
	[charges] [int] NOT NULL,
	[attuneType] [int] NOT NULL,
	[timeCreated] [datetime] NOT NULL,
	[whoCreated] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO

INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 1, 19000, 0, 0, N'', 50, -1, 0, CAST(0x0000982A0053379E AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 2, 1020, 0, 0, N'', 1000, -1, 0, CAST(0x0000981A00AD6812 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 3, 800, 0, 0, N'', 15, -1, 0, CAST(0x0000981A00F832E1 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 4, 15015, 0, 0, N'', 75, -1, 0, CAST(0x000098180090645E AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 5, 8100, 0, 0, N'', 50, -1, 0, CAST(0x0000980B01134718 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 6, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011834E9 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 7, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011834EE AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 8, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011834F3 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 9, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011834FC AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 10, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011834FC AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 11, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A01183501 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 12, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A01183506 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 13, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A0118350A AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 14, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A0118350F AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 15, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A01183518 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 16, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A0118351D AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 17, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A01183522 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 18, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A01183526 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 19, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A0118352B AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerWearing] ([playerID], [wearingSlot], [itemID], [attunedID], [wearOrientation], [special], [coinvalue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 20, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A01183530 AS DateTime), N'SYSTEM')

/****** Object:  Table [dbo].[PlayerSpells]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerSpells](
	[SpellListID] [int] IDENTITY(1,1) NOT NULL,
	[PlayerID] [int] NOT NULL,
	[SpellSlot] [int] NULL,
	[SpellID] [int] NULL,
	[ChantString] [nvarchar](255) NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PlayerSpells] ON

INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (26, 1, 1, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (27, 1, 2, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (28, 1, 3, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (29, 1, 4, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (30, 1, 5, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (31, 1, 6, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (32, 1, 7, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (33, 1, 8, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (34, 1, 9, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (35, 1, 10, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (36, 1, 11, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (37, 1, 12, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (38, 1, 13, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (39, 1, 14, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (40, 1, 15, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (41, 1, 16, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (42, 1, 17, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (43, 1, 18, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (44, 1, 19, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (45, 1, 20, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (46, 1, 21, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (47, 1, 22, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (48, 1, 23, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (49, 1, 24, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (50, 1, 25, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (51, 1, 26, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (52, 1, 27, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (53, 1, 28, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (54, 1, 29, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (55, 1, 30, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (56, 1, 31, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (57, 1, 32, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (58, 1, 33, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (59, 1, 34, -1, NULL)
INSERT [dbo].[PlayerSpells] ([SpellListID], [PlayerID], [SpellSlot], [SpellID], [ChantString]) VALUES (60, 1, 35, -1, NULL)

SET IDENTITY_INSERT [dbo].[PlayerSpells] OFF
/****** Object:  Table [dbo].[PlayerSkills]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerSkills](
	[playerID] [int] NULL,
	[mace] [bigint] NULL,
	[bow] [bigint] NULL,
	[flail] [bigint] NULL,
	[dagger] [bigint] NULL,
	[rapier] [bigint] NULL,
	[twoHanded] [bigint] NULL,
	[staff] [bigint] NULL,
	[shuriken] [bigint] NULL,
	[sword] [bigint] NULL,
	[threestaff] [bigint] NULL,
	[halberd] [bigint] NULL,
	[unarmed] [bigint] NULL,
	[thievery] [bigint] NULL,
	[magic] [bigint] NULL,
	[bash] [bigint] NULL,
	[highMace] [bigint] NULL,
	[highBow] [bigint] NULL,
	[highFlail] [bigint] NULL,
	[highDagger] [bigint] NULL,
	[highRapier] [bigint] NULL,
	[highTwoHanded] [bigint] NULL,
	[highStaff] [bigint] NULL,
	[highShuriken] [bigint] NULL,
	[highSword] [bigint] NULL,
	[highThreestaff] [bigint] NULL,
	[highHalberd] [bigint] NULL,
	[highUnarmed] [bigint] NULL,
	[highThievery] [bigint] NULL,
	[highMagic] [bigint] NULL,
	[highBash] [bigint] NULL,
	[trainedMace] [bigint] NULL,
	[trainedBow] [bigint] NULL,
	[trainedFlail] [bigint] NULL,
	[trainedDagger] [bigint] NULL,
	[trainedRapier] [bigint] NULL,
	[trainedTwoHanded] [bigint] NULL,
	[trainedStaff] [bigint] NULL,
	[trainedShuriken] [bigint] NULL,
	[trainedSword] [bigint] NULL,
	[trainedThreestaff] [bigint] NULL,
	[trainedHalberd] [bigint] NULL,
	[trainedUnarmed] [bigint] NULL,
	[trainedThievery] [bigint] NULL,
	[trainedMagic] [bigint] NULL,
	[trainedBash] [bigint] NULL
) ON [PRIMARY]
GO

INSERT [dbo].[PlayerSkills] ([playerID], [mace], [bow], [flail], [dagger], [rapier], [twoHanded], [staff], [shuriken], [sword], [threestaff], [halberd], [unarmed], [thievery], [magic], [bash], [highMace], [highBow], [highFlail], [highDagger], [highRapier], [highTwoHanded], [highStaff], [highShuriken], [highSword], [highThreestaff], [highHalberd], [highUnarmed], [highThievery], [highMagic], [highBash], [trainedMace], [trainedBow], [trainedFlail], [trainedDagger], [trainedRapier], [trainedTwoHanded], [trainedStaff], [trainedShuriken], [trainedSword], [trainedThreestaff], [trainedHalberd], [trainedUnarmed], [trainedThievery], [trainedMagic], [trainedBash]) VALUES (1, 0, 31, 4800, 31, 0, 0, 0, 0, 2400, 0, 5244, 0, 0, 0, 0, 0, 31, 4800, 31, 0, 0, 0, 0, 2400, 0, 5244, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 37, 0, 0, 0, 0)

/****** Object:  Table [dbo].[PlayerSettings]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerSettings](
	[playerID] [int] NULL,
	[anonymous] [bit] NOT NULL,
	[echo] [bit] NOT NULL,
	[filterProfanity] [bit] NOT NULL,
	[friendsList] [nvarchar](max) NOT NULL,
	[friendNotify] [bit] NOT NULL,
	[ignoreList] [nvarchar](max) NOT NULL,
	[immortal] [bit] NOT NULL,
	[invisible] [bit] NOT NULL,
	[receiveGroupInvites] [bit] NOT NULL,
	[receivePages] [bit] NOT NULL,
	[receiveTells] [bit] NOT NULL,
	[showStaffTitle] [bit] NOT NULL,
	[macros] [nvarchar](max) NULL,
	[displayCombatDamage] [bit] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

INSERT [dbo].[PlayerSettings] ([playerID], [anonymous], [echo], [filterProfanity], [friendsList], [friendNotify], [ignoreList], [immortal], [invisible], [receiveGroupInvites], [receivePages], [receiveTells], [showStaffTitle], [macros], [displayCombatDamage]) VALUES (1, 0, 1, 1, N'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0', 1, N'0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0', 0, 0, 1, 1, 1, 1, N'', 0)

/****** Object:  Table [dbo].[PlayerSack]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlayerSack](
	[SackID] [int] IDENTITY(1,1) NOT NULL,
	[PlayerID] [int] NOT NULL,
	[SackSlot] [int] NULL,
	[SackItem] [int] NULL,
	[Attuned] [int] NULL,
	[SackGold] [float] NULL,
	[Special] [varchar](255) NOT NULL,
	[CoinValue] [float] NOT NULL,
	[Charges] [int] NOT NULL,
	[Venom] [int] NOT NULL,
	[attuneType] [int] NOT NULL,
	[FigExp] [bigint] NOT NULL,
	[TimeCreated] [datetime] NOT NULL,
	[WhoCreated] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PlayerSack] ON

INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (1, 1, 1, 239, 0, 0, N'', 13, -1, 0, 0, 0, CAST(0x0000982A0053379E AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (2, 1, 2, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000982A0071C17D AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (3, 1, 3, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000981C00C7C6DC AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (4, 1, 4, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000981B01849010 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (5, 1, 5, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000981B016DD159 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (6, 1, 6, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000981B016DD162 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (7, 1, 7, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A01183487 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (8, 1, 8, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A0118348C AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (9, 1, 9, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A01183490 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (10, 1, 10, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A01183495 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (11, 1, 11, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A0118349A AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (12, 1, 12, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834A3 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (13, 1, 13, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834A8 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (14, 1, 14, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834AC AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (15, 1, 15, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834B1 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (16, 1, 16, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834B6 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (17, 1, 17, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834BB AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (18, 1, 18, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834BF AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (19, 1, 19, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834C4 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (20, 1, 20, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834C9 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerSack] ([SackID], [PlayerID], [SackSlot], [SackItem], [Attuned], [SackGold], [Special], [CoinValue], [Charges], [Venom], [attuneType], [FigExp], [TimeCreated], [WhoCreated]) VALUES (21, 1, 21, 0, 0, 0, N'', 0, 0, 0, 0, 0, CAST(0x0000990A011834CD AS DateTime), N'SYSTEM')

SET IDENTITY_INSERT [dbo].[PlayerSack] OFF
/****** Object:  Table [dbo].[PlayerRings]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlayerRings](
	[ringID] [int] IDENTITY(1,1) NOT NULL,
	[playerID] [int] NOT NULL,
	[ringFinger] [int] NULL,
	[itemID] [int] NULL,
	[attunedID] [int] NULL,
	[isRecall] [bit] NOT NULL,
	[wasRecall] [bit] NOT NULL,
	[recallLand] [smallint] NULL,
	[recallMap] [smallint] NULL,
	[recallX] [int] NULL,
	[recallY] [int] NULL,
	[recallZ] [int] NOT NULL,
	[special] [varchar](255) NOT NULL,
	[coinValue] [float] NOT NULL,
	[charges] [int] NOT NULL,
	[attuneType] [int] NOT NULL,
	[timeCreated] [datetime] NOT NULL,
	[whoCreated] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PlayerRings] ON

INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835A5 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (2, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835A5 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (3, 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835AA AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (4, 1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835AA AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (5, 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835AE AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (6, 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835AE AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (7, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835B3 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerRings] ([ringID], [playerID], [ringFinger], [itemID], [attunedID], [isRecall], [wasRecall], [recallLand], [recallMap], [recallX], [recallY], [recallZ], [special], [coinValue], [charges], [attuneType], [timeCreated], [whoCreated]) VALUES (8, 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, CAST(0x0000990A011835B3 AS DateTime), N'SYSTEM')

SET IDENTITY_INSERT [dbo].[PlayerRings] OFF
/****** Object:  Table [dbo].[PlayerQuests]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlayerQuests](
	[keyID] [int] IDENTITY(1,1) NOT NULL,
	[playerID] [int] NULL,
	[questID] [int] NULL,
	[timesCompleted] [int] NULL,
	[startDate] [varchar](50) NULL,
	[finishDate] [varchar](50) NULL,
	[currentStep] [smallint] NULL,
 CONSTRAINT [PK_PlayerQuests] PRIMARY KEY CLUSTERED 
(
	[keyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PlayerQuests] ON

SET IDENTITY_INSERT [dbo].[PlayerQuests] OFF
/****** Object:  Table [dbo].[PlayerLocker]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlayerLocker](
	[lockerID] [int] IDENTITY(1,1) NOT NULL,
	[playerID] [int] NOT NULL,
	[lockerSlot] [int] NULL,
	[itemID] [int] NULL,
	[attunedID] [int] NULL,
	[special] [varchar](255) NOT NULL,
	[charges] [int] NOT NULL,
	[coinValue] [float] NOT NULL,
	[venom] [int] NOT NULL,
	[figExp] [bigint] NOT NULL,
	[attuneType] [int] NOT NULL,
	[nocked] [bit] NOT NULL,
	[timeCreated] [datetime] NOT NULL,
	[whoCreated] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PlayerLocker] ON

INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (1, 1, 1, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183534 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (2, 1, 2, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183539 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (3, 1, 3, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118353E AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (4, 1, 4, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183542 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (5, 1, 5, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183547 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (6, 1, 6, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118354C AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (7, 1, 7, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183551 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (8, 1, 8, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183555 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (9, 1, 9, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118355A AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (10, 1, 10, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118355F AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (11, 1, 11, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183563 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (12, 1, 12, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183568 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (13, 1, 13, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118356D AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (14, 1, 14, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183571 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (15, 1, 15, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183576 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (16, 1, 16, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118357B AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (17, 1, 17, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118357F AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (18, 1, 18, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183584 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (19, 1, 19, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183589 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerLocker] ([lockerID], [playerID], [lockerSlot], [itemID], [attunedID], [special], [charges], [coinValue], [venom], [figExp], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (20, 1, 20, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118358D AS DateTime), N'SYSTEM')

SET IDENTITY_INSERT [dbo].[PlayerLocker] OFF
/****** Object:  Table [dbo].[PlayerHeld]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerHeld](
	[playerID] [int] NULL,
	[rightHand] [bit] NULL,
	[itemID] [int] NULL,
	[attunedID] [int] NULL,
	[special] [nvarchar](1000) NULL,
	[charges] [int] NULL,
	[coinValue] [bigint] NULL,
	[figExp] [bigint] NULL,
	[venom] [int] NULL,
	[attuneType] [int] NOT NULL,
	[nocked] [bit] NOT NULL,
	[timeCreated] [datetime] NULL,
	[whoCreated] [nvarchar](100) NULL
) ON [PRIMARY]
GO

INSERT [dbo].[PlayerHeld] ([playerID], [rightHand], [itemID], [attunedID], [special], [charges], [coinValue], [figExp], [venom], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (1, 0, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000980C009CCB8C AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerHeld] ([playerID], [rightHand], [itemID], [attunedID], [special], [charges], [coinValue], [figExp], [venom], [attuneType], [nocked], [timeCreated], [whoCreated]) VALUES (1, 1, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000981800906492 AS DateTime), N'SYSTEM')

/****** Object:  Table [dbo].[PlayerFlags]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerFlags](
	[keyID] [int] IDENTITY(1,1) NOT NULL,
	[playerID] [int] NULL,
	[questFlags] [nvarchar](max) NULL,
	[contentFlags] [nvarchar](max) NULL,
 CONSTRAINT [PK_PlayerFlags] PRIMARY KEY CLUSTERED 
(
	[keyID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PlayerFlags] ON

INSERT [dbo].[PlayerFlags] ([keyID], [playerID], [questFlags], [contentFlags]) VALUES (1, 1, N'', N'')

SET IDENTITY_INSERT [dbo].[PlayerFlags] OFF
/****** Object:  Table [dbo].[PlayerEffects]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PlayerEffects](
	[EffectListID] [int] IDENTITY(1,1) NOT NULL,
	[PlayerID] [int] NOT NULL,
	[EffectSlot] [int] NULL,
	[EffectID] [int] NULL,
	[Amount] [int] NULL,
	[Duration] [int] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PlayerEffects] ON

INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (01, 1, 1, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (02, 1, 2, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (03, 1, 3, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (04, 1, 4, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (05, 1, 5, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (06, 1, 6, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (07, 1, 7, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (08, 1, 8, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (09, 1, 9, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (10, 1, 10, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (11, 1, 11, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (12, 1, 12, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (13, 1, 13, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (14, 1, 14, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (15, 1, 15, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (16, 1, 16, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (17, 1, 17, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (18, 1, 18, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (19, 1, 19, 0, 0, 0)
INSERT [dbo].[PlayerEffects] ([EffectListID], [PlayerID], [EffectSlot], [EffectID], [Amount], [Duration]) VALUES (20, 1, 20, 0, 0, 0)

SET IDENTITY_INSERT [dbo].[PlayerEffects] OFF
/****** Object:  Table [dbo].[PlayerBelt]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PlayerBelt](
	[beltID] [int] IDENTITY(1,1) NOT NULL,
	[playerID] [int] NOT NULL,
	[beltSlot] [int] NULL,
	[itemID] [int] NULL,
	[attunedID] [int] NULL,
	[special] [varchar](255) NOT NULL,
	[coinValue] [float] NOT NULL,
	[charges] [int] NOT NULL,
	[venom] [int] NOT NULL,
	[attuneType] [int] NOT NULL,
	[figExp] [bigint] NOT NULL,
	[nocked] [bit] NOT NULL,
	[timeCreated] [datetime] NOT NULL,
	[whoCreated] [nvarchar](100) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PlayerBelt] ON

INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (1, 1, 1, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183592 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (2, 1, 2, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183597 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (3, 1, 3, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A01183597 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (4, 1, 4, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118359C AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (5, 1, 5, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A0118359C AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (6, 1, 6, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A011835A0 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (7, 1, 7, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A011835A0 AS DateTime), N'SYSTEM')
INSERT [dbo].[PlayerBelt] ([beltID], [playerID], [beltSlot], [itemID], [attunedID], [special], [coinValue], [charges], [venom], [attuneType], [figExp], [nocked], [timeCreated], [whoCreated]) VALUES (8, 1, 8, 0, 0, N'', 0, 0, 0, 0, 0, 0, CAST(0x0000990A011835A0 AS DateTime), N'SYSTEM')

SET IDENTITY_INSERT [dbo].[PlayerBelt] OFF
/****** Object:  Table [dbo].[Player]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Player](
	[playerID] [int] IDENTITY(1,1) NOT NULL,
	[notes] [text] NULL,
	[accountID] [int] NOT NULL,
	[account] [nvarchar](20) NULL,
	[name] [nvarchar](255) NULL,
	[gender] [smallint] NULL,
	[race] [varchar](50) NULL,
	[classFullName] [varchar](50) NULL,
	[classType] [varchar](50) NULL,
	[visualKey] [varchar](50) NULL,
	[alignment] [int] NULL,
	[confRoom] [int] NOT NULL,
	[ImpLevel] [int] NULL,
	[ancestor] [bit] NOT NULL,
	[ancestorID] [int] NOT NULL,
	[facet] [smallint] NULL,
	[land] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
	[dirPointer] [char](1) NULL,
	[stunned] [smallint] NOT NULL,
	[floating] [smallint] NOT NULL,
	[dead] [bit] NOT NULL,
	[fighterSpecialization] [varchar](50) NOT NULL,
	[level] [smallint] NOT NULL,
	[exp] [bigint] NOT NULL,
	[hits] [int] NOT NULL,
	[hitsMax] [int] NOT NULL,
	[hitsAdjustment] [int] NOT NULL,
	[hitsDoctored] [int] NOT NULL,
	[stamina] [int] NOT NULL,
	[stamLeft] [int] NOT NULL,
	[staminaAdjustment] [int] NOT NULL,
	[mana] [int] NOT NULL,
	[manaMax] [int] NOT NULL,
	[manaAdjustment] [int] NOT NULL,
	[age] [int] NOT NULL,
	[roundsPlayed] [bigint] NOT NULL,
	[numKills] [bigint] NOT NULL,
	[numDeaths] [bigint] NOT NULL,
	[bankGold] [float] NOT NULL,
	[strength] [int] NOT NULL,
	[dexterity] [int] NOT NULL,
	[intelligence] [int] NOT NULL,
	[wisdom] [int] NOT NULL,
	[constitution] [int] NOT NULL,
	[charisma] [int] NOT NULL,
	[strengthAdd] [int] NOT NULL,
	[dexterityAdd] [int] NOT NULL,
	[birthday] [datetime] NOT NULL,
	[lastOnline] [datetime] NOT NULL,
	[deleteDate] [datetime] NOT NULL,
	[currentKarma] [int] NOT NULL,
	[lifetimeKarma] [bigint] NOT NULL,
	[lifetimeMarks] [bigint] NOT NULL,
	[pvpKills] [bigint] NOT NULL,
	[pvpDeaths] [bigint] NOT NULL,
	[playersKilled] [nvarchar](4000) NULL,
	[playersFlagged] [nvarchar](1000) NULL,
	[UW_hitsMax] [int] NOT NULL,
	[UW_hitsAdjustment] [int] NOT NULL,
	[UW_staminaMax] [int] NOT NULL,
	[UW_staminaAdjustment] [int] NOT NULL,
	[UW_manaMax] [int] NOT NULL,
	[UW_manaAdjustment] [int] NOT NULL,
	[UW_intestines] [bit] NOT NULL,
	[UW_liver] [bit] NOT NULL,
	[UW_lungs] [bit] NOT NULL,
	[UW_stomach] [bit] NOT NULL,
 CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[playerID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Player', @level2type=N'COLUMN',@level2name=N'ancestor'
GO
SET IDENTITY_INSERT [dbo].[Player] ON

INSERT [dbo].[Player] ([playerID], [notes], [accountID], [account], [name], [gender], [race], [classFullName], [classType], [visualKey], [alignment], [confRoom], [ImpLevel], [ancestor], [ancestorID], [facet], [land], [map], [xCord], [yCord], [zCord], [dirPointer], [stunned], [floating], [dead], [fighterSpecialization], [level], [exp], [hits], [hitsMax], [hitsAdjustment], [hitsDoctored], [stamina], [stamLeft], [staminaAdjustment], [mana], [manaMax], [manaAdjustment], [age], [roundsPlayed], [numKills], [numDeaths], [bankGold], [strength], [dexterity], [intelligence], [wisdom], [constitution], [charisma], [strengthAdd], [dexterityAdd], [birthday], [lastOnline], [deleteDate], [currentKarma], [lifetimeKarma], [lifetimeMarks], [pvpKills], [pvpDeaths], [playersKilled], [playersFlagged], [UW_hitsMax], [UW_hitsAdjustment], [UW_staminaMax], [UW_staminaAdjustment], [UW_manaMax], [UW_manaAdjustment], [UW_intestines], [UW_liver], [UW_lungs], [UW_stomach]) VALUES (1, N'', 1, N'test1234', N'TestOneTwo', 1, N'Illyria', N'Fighter', N'Fighter', N'male_fighter_pc_brown', 1, 0, 0, 0, 0, 0, 0, 0, 42, 27, 0, N'v', 0, 3, 0, N'None', 4, 4996, 36, 36, 0, 0, 10, 10, 0, 0, 0, 0, 806, 806, 8, 0, 0, 17, 18, 10, 17, 8, 18, 1, 1, CAST(0x0000A4FC0001D163 AS DateTime), CAST(0x0000A4FC001A5F6A AS DateTime), CAST(0x0000979200000000 AS DateTime), 0, 0, 0, 0, 0, N'', N'', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)

SET IDENTITY_INSERT [dbo].[Player] OFF
/****** Object:  Table [dbo].[NPCLocation]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NPCLocation](
	[NPCIndex] [int] NOT NULL,
	[active] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[facet] [smallint] NOT NULL,
	[land] [smallint] NOT NULL,
	[map] [smallint] NOT NULL,
	[xCord] [smallint] NOT NULL,
	[yCord] [smallint] NOT NULL,
	[zCord] [int] NOT NULL,
 CONSTRAINT [PK_NPCLocation] PRIMARY KEY CLUSTERED 
(
	[NPCIndex] ASC,
	[facet] ASC,
	[land] ASC,
	[map] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[fnIsColumnPrimaryKey]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE   FUNCTION [dbo].[fnIsColumnPrimaryKey](@sTableName varchar(128), @nColumnName varchar(128))
RETURNS bit
AS
BEGIN
	DECLARE @nTableID int,
		@nIndexID int,
		@i int
	
	SET 	@nTableID = OBJECT_ID(@sTableName)
	
	SELECT 	@nIndexID = indid
	FROM 	sysindexes
	WHERE 	id = @nTableID
	 AND 	indid BETWEEN 1 And 254 
	 AND 	(status & 2048) = 2048
	
	IF @nIndexID Is Null
		RETURN 0
	
	IF @nColumnName IN
		(SELECT sc.[name]
		FROM 	sysindexkeys sik
			INNER JOIN syscolumns sc ON sik.id = sc.id AND sik.colid = sc.colid
		WHERE 	sik.id = @nTableID
		 AND 	sik.indid = @nIndexID)
	 BEGIN
		RETURN 1
	 END


	RETURN 0
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_Area_Select_All]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------
-- Select All records from Areas
--------------------------------------------------

CREATE PROC [dbo].[prApp_Area_Select_All]
AS
SELECT *
FROM Areas
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Update_Field]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Update a single field in an Account record
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Account_Update_Field]

	@accountID int = NULL,
	@field nvarchar(50) = NULL,
	@type nvarchar(50) = NULL,
	@int int = NULL,
	@bit bit = NULL,
	@char char = NULL,
	@string nvarchar(4000) = NULL,
	@dateTime datetime = NULL
	
AS

if @type = 'System.Int32'
	EXEC ('UPDATE Account SET ' + @field + ' = ' + @int + ' WHERE accountID = ' + @accountID)
else if @type = 'System.Boolean'
	EXEC ('UPDATE Account SET ' + @field + ' = ' + @bit + ' WHERE accountID = ' + @accountID)
else if @type = 'System.Char'
	EXEC ('UPDATE Account SET ' + @field + ' = ' + @char + ' WHERE accountID = ' + @accountID)
else if @type = 'System.String'
	EXEC ('UPDATE Account SET ' + @field + ' = "' + @string + '" WHERE accountID = ' + @accountID)
else if @type = 'System.DateTime'
	EXEC ('UPDATE Account SET ' + @field + ' = "' + @dateTime + '" WHERE accountID = ' + @accountID)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Update_Field]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
-- quoted identifier OFF

----------------------------------------------------------------------------
-- Update a single variable in a Player record
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Update_Field]

	@table nvarchar(50) = NULL,
	@playerID int = NULL,
	@field nvarchar(50) = NULL,
	@type nvarchar(50) = NULL,
	@short smallint = NULL,
	@int int = NULL,
	@long bigint = NULL,
	@float float = NULL,
	@bit bit = NULL,
	@char char = NULL,
	@string nvarchar(4000) = NULL,
	@dateTime datetime = NULL
	
AS

if @type = 'System.Int16'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = ' + @short + ' WHERE playerID = ' + @playerID)
else if @type = 'System.Int32'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = ' + @int + ' WHERE playerID = ' + @playerID)
else if @type = 'System.Int64'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = ' + @long + ' WHERE playerID = ' + @playerID)
else if @type = 'System.Boolean'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = ' + @bit + ' WHERE playerID = ' + @playerID)
else if @type = 'System.Char'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = ' + @char + ' WHERE playerID = ' + @playerID)
else if @type = 'System.String'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = "' + @string + '" WHERE playerID = ' + @playerID)
else if @type = 'System.DateTime'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = "' + @dateTime + '" WHERE playerID = ' + @playerID)
else if @type = 'System.Double'
	EXEC ('UPDATE ' + @table + ' SET ' + @field + ' = "' + @float + '" WHERE playerID = ' + @playerID)
GO
/****** Object:  StoredProcedure [dbo].[prApp_SpawnZoneLink_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record in SpawnZoneLink
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_SpawnZoneLink_Update]
	@Enabled bit,
	@SpawnZoneID int,
	@NPCID int,
	@SpawnTimer int,
	@MaxAllowedInZone int,
	@SpawnMessage varchar(255) = NULL,
	@SinglePoint bit,
	@MinZone int = NULL,
	@MaxZone int = NULL,
	@NPCList varchar(255) = NULL,
	@spawnLand int,
	@spawnMap int,
	@spawnXcord int,
	@spawnYcord int,
	@spawnRadius int
AS

UPDATE	SpawnZoneLink
SET
	Enabled = @Enabled,
	NPCID = @NPCID,
	SpawnTimer = @SpawnTimer,
	MaxAllowedInZone = @MaxAllowedInZone,
	SpawnMessage = @SpawnMessage,
	SinglePoint = @SinglePoint,
	MinZone = @MinZone,
	MaxZone = @MaxZone,
	NPCList = @NPCList,
	spawnLand = @spawnLand,
	spawnMap = @spawnMap,
	spawnXcord = @spawnXcord,
	spawnYcord = @spawnYcord,
	spawnRadius = @spawnRadius

WHERE 	SpawnZoneID = @SpawnZoneID
GO
/****** Object:  StoredProcedure [dbo].[prApp_SpawnZoneLink_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Select a single record from SpawnZoneLink
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_SpawnZoneLink_Select]
	@SpawnZoneLinkID int
AS

SELECT	SpawnZoneLinkID,
	NPCID,
	SpawnTimer,
	MaxAllowedInZone,
	SpawnMessage,
	SinglePoint,
	MinZone,
	MaxZone,
	NPCList
FROM	SpawnZoneLink
WHERE 	SpawnZoneLinkID = @SpawnZoneLinkID
GO
/****** Object:  StoredProcedure [dbo].[prApp_SpawnZoneLink_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into SpawnZoneLink
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_SpawnZoneLink_Insert]
	@Enabled int,
	@NPCID int,
	@SpawnTimer int,
	@MaxAllowedInZone int,
	@SpawnMessage varchar(255) = NULL,
	@SinglePoint bit,
	@MinZone int = NULL,
	@MaxZone int = NULL,
	@NPCList varchar(255) = NULL,
	@spawnLand int,
	@spawnMap int,
	@spawnXcord int,
	@spawnYcord int,
	@spawnRadius int
AS

INSERT SpawnZoneLink(Enabled, NPCID, SpawnTimer, MaxAllowedInZone, SpawnMessage, SinglePoint, MinZone, MaxZone, NPCList, spawnLand, spawnMap, spawnXcord, spawnYcord, spawnRadius)
VALUES (@Enabled, @NPCID, @SpawnTimer, @MaxAllowedInZone, @SpawnMessage, @SinglePoint, @MinZone, @MaxZone, @NPCList, @spawnLand, @spawnMap, @spawnXcord, @spawnYcord, @spawnRadius)
GO
/****** Object:  StoredProcedure [dbo].[prApp_SpawnZoneLink_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Delete a single record from SpawnZoneLink
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_SpawnZoneLink_Delete]
	@SpawnZoneID int
AS

DELETE	SpawnZoneLink
WHERE 	SpawnZoneID = @SpawnZoneID
GO
/****** Object:  StoredProcedure [dbo].[prApp_SpawnZone_Select_By_Map]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------
-- Select a group of records from NPC where Difficulty is the restriction
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_SpawnZone_Select_By_Map]
	@map int
AS

SELECT	*
FROM	SpawnZone
WHERE 	SpawnMap = @map
GO
/****** Object:  StoredProcedure [dbo].[prApp_SpawnZone_Select_All]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from SpawnZoneLink
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_SpawnZone_Select_All]
AS

SELECT	*
FROM	SpawnZone
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into Player
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Update]

	@playerID int,
	@notes text = NULL,
	@account nvarchar(20)=NULL,
	@name nvarchar(20)=NULL,
	@gender smallint= NULL,
	@race varchar(50) = NULL,
	@classFullName varchar(50)=NULL,
	@classType varchar(50)= NULL,
	@visualKey varchar(50)= NULL,
	@alignment int= NULL,
	@confRoom int= NULL,
	@impLevel int= NULL,
	@ancestor bit,
	@ancestorID int= NULL,
	@land smallint= NULL,
	@map smallint= NULL,
	@xCord smallint= NULL,
	@yCord smallint= NULL,
	@zCord int = NULL,
	@dirPointer char(1)=NULL,
	@stunned smallint = NULL,
	@floating smallint = NULL,
	@dead bit,
	@fighterSpecialization varchar(50) = NULL,
	@level smallint = NULL,
	@exp bigint = NULL,
	@hits int = NULL,
	@hitsMax int = NULL,
	@hitsAdjustment int = NULL,
	@hitsDoctored int = NULL,
	@stamina int = NULL,
	@stamLeft int = NULL,
	@staminaAdjustment int = NULL,
	@mana int = NULL,
	@manaMax int = NULL,
	@manaAdjustment int = NULL,
	@age int = NULL,
	@roundsPlayed bigint = NULL,
	@numKills int = NULL,
	@numDeaths int = NULL,
	@bankGold float = NULL,
	@strength int = NULL,
	@dexterity int = NULL,
	@intelligence int = NULL,
	@wisdom int = NULL,
	@constitution int = NULL,
	@charisma int = NULL,
	@strengthAdd int = NULL,
	@dexterityAdd int = NULL,
	@lastOnline datetime = NULL,
	@UW_hitsMax int = NULL,
	@UW_hitsAdjustment int = NULL,
	@UW_staminaMax int = NULL,
	@UW_staminaAdjustment int = NULL,
	@UW_manaMax int = NULL,
	@UW_manaAdjustment int = NULL,
	@UW_intestines bit,
	@UW_liver bit,
	@UW_lungs bit,
	@UW_stomach bit,
	@currentKarma int = NULL,
	@lifetimeKarma bigint = NULL,
	@lifetimeMarks int = NULL,
	@pvpDeaths bigint = NULL,
	@pvpKills bigint = NULL,
	@playersFlagged nvarchar(4000) = NULL,
	@playersKilled nvarchar(1000) = NULL
AS
UPDATE Player
SET      
	account = @account,
	notes = @notes,
	[name] = @name,
	gender = @gender,
	race = @race,
	classFullName = @classFullName,
	classType = @classType,
	visualKey = @visualKey,
	alignment = @alignment,
	confRoom = @confRoom,
	impLevel = @ImpLevel,
	ancestor = @ancestor,
	ancestorID = @ancestorID,
	land = @land,
	map = @map,
	xCord = @xCord,
	yCord = @yCord,
	zCord = @zCord,
	dirPointer = @dirPointer,
	stunned = @stunned,
	floating = @floating,
	dead = @dead,
	fighterSpecialization = @fighterSpecialization,
	[level] = @level,
	[exp] = @exp,
	hits = @hits,
	hitsMax = @hitsMax,
	hitsAdjustment = @hitsAdjustment,
	hitsDoctored = @hitsDoctored,
	stamina = @stamina,
	stamLeft = @stamLeft,
	staminaAdjustment = @staminaAdjustment,
	mana = @mana,
	manaMax = @manaMax,
	manaAdjustment = @manaAdjustment,
	age = @age,
	roundsPlayed = @roundsPlayed,
	numKills = @numKills,
	numDeaths = @numDeaths,
	bankGold = @bankGold,
	strength = @strength,
	dexterity = @dexterity,
	intelligence = @intelligence,
	wisdom = @wisdom,
	constitution = @constitution,
	charisma = @charisma,
	strengthAdd = @strengthAdd,
	dexterityAdd = @dexterityAdd,
	lastOnline = @lastOnline,
	UW_hitsMax = @UW_hitsMax,
	UW_hitsAdjustment = @UW_hitsAdjustment,
	UW_staminaMax = @UW_staminaMax,
	UW_staminaAdjustment = @UW_staminaAdjustment,
	UW_manaMax = @UW_manaMax,
	UW_manaAdjustment = @UW_manaAdjustment,
	UW_intestines = @UW_intestines,
	UW_liver = @UW_liver,
	UW_lungs = @UW_lungs,
	UW_stomach = @UW_stomach,
	currentKarma = @currentKarma,
	lifetimeKarma = @lifetimeKarma,
	lifetimeMarks = @lifetimeMarks,
	pvpDeaths = @pvpDeaths,
	pvpKills = @pvpKills,
	playersFlagged = @playersFlagged,
	playersKilled = @playersKilled

WHERE
playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record in Account
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Account_Update]

	@accountID int,
	@notes text = NULL,
	@account nvarchar(20) = NULL,
	@password nvarchar(20) = NULL,
	@currentMarks int,
	@email nvarchar(50)
	
AS
UPDATE Account
SET
	notes = @notes,
	account = @account,
	password = @password,
	currentMarks = @currentMarks,
	email = @email

WHERE
accountID = @accountID
GO
/****** Object:  UserDefinedFunction [dbo].[fnColumnDefault]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fnColumnDefault](@sTableName varchar(128), @sColumnName varchar(128))
RETURNS varchar(4000)
AS
BEGIN
	DECLARE @sDefaultValue varchar(4000)

	SELECT	@sDefaultValue = dbo.fnCleanDefaultValue(COLUMN_DEFAULT)
	FROM	INFORMATION_SCHEMA.COLUMNS
	WHERE	TABLE_NAME = @sTableName
	 AND 	COLUMN_NAME = @sColumnName

	RETURN 	@sDefaultValue

END
GO
/****** Object:  StoredProcedure [dbo].[prApp_Stores_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record in Stores
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Stores_Update]
	@stockID int,
	@stocked smallint
AS

UPDATE	Stores
SET	stocked = @stocked
WHERE 	stockID = @stockID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Stores_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all items from Stores
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Stores_Select]
	@NPCIdent int
AS

SELECT	stockID, NPCIdent, original, itemID, sellPrice, stocked, charges, figExp, restock, seller
FROM	Stores
WHERE 	NPCIdent = @NPCIdent
GO
/****** Object:  StoredProcedure [dbo].[prApp_Stores_Restock]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Restock items in the Stores table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Stores_Restock]

AS
UPDATE    Stores
SET              stocked = restock
WHERE     (original = 1) AND stocked <> restock
GO
/****** Object:  StoredProcedure [dbo].[prApp_Stores_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into Stores
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Stores_Insert]
	@NPCIdent int,
	@original bit = false,
	@itemID int,
	@sellPrice int,
	@stocked smallint,
	@charges smallint,
	@figExp bigint,
	@seller varchar(20)
AS

INSERT Stores(NPCIdent, original, itemID, sellPrice, stocked, charges, figExp, seller)
VALUES (@NPCIdent, @original, @itemID, @sellPrice, @stocked, @charges, @figExp, @seller)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Stores_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from Stores
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Stores_Delete]
	@stockID int
AS

DELETE	Stores
WHERE 	stockID = @stockID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Stores_Clear]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Clear all items from Stores that are not Original
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Stores_Clear]

AS

DELETE	Stores
WHERE 	(original = 0)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Spells_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all spells from Spells
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Spells_Select]

AS

SELECT	*
FROM	Spells
GO
/****** Object:  StoredProcedure [dbo].[prApp_Spells_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into Spell
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Spells_Insert]
	@command varchar(20),
	@name varchar(255),
	@description text,
	@methodName varchar(255),
	@spellType int,
	@classTypes varchar(MAX),
	@requiredLevel int,
	@trainingPrice int,
	@manaCost int
AS

INSERT Spells(command, [name], description, methodName, spellType, classTypes,
requiredLevel, trainingPrice, manaCost)
VALUES (@command, @name, @description, @methodName, @spellType, @classTypes,
@requiredLevel, @trainingPrice, @manaCost)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Spells_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from Spell
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Spells_Delete]
	@spellID int
AS

DELETE	Spells
WHERE 	spellID = @spellID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Scores_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[prApp_Scores_Select]
	@devRequest bit,
	@classType varchar(50)

AS
if @devRequest = 1
	if @classType <> 'None'
		SELECT [playerID], land, [name], classType, classFullName, [level], [exp], numKills, roundsPlayed, lastOnline, pvpKills, pvpDeaths, impLevel
		FROM Player WHERE classType = @classType
		ORDER By [exp] DESC
	else
		SELECT [playerID], land, [name], classType, classFullName, [level], [exp], numKills, roundsPlayed, lastOnline, pvpKills, pvpDeaths, impLevel
		FROM Player
		ORDER By [exp] DESC

else
	if @classType <> 'None'
		SELECT [playerID], land, [name], classType, classFullName, [level], [exp], numKills, roundsPlayed, lastOnline, pvpKills, pvpDeaths, impLevel
		FROM Player WHERE classType = @classType AND impLevel = 0
		ORDER By [exp] DESC
	else
		SELECT [playerID], land, [name], classType, classFullName, [level], [exp], numKills, roundsPlayed, lastOnline, pvpKills, pvpDeaths, impLevel
		FROM Player WHERE impLevel = 0
		ORDER By [exp] DESC
GO
/****** Object:  StoredProcedure [dbo].[prApp_Quest_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from Quest
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Quest_Select]
AS

SELECT	*
FROM	Quest
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerWearing_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerWearing
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerWearing_Update]
	@playerID int,
	@wearingSlot int,
	@itemID int,
	@attunedID int,
	@wearOrientation int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@attuneType bit,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
	
AS
UPDATE PlayerWearing
SET      
	itemID = @itemID,
	attunedID = @attunedID,
	wearOrientation = @wearOrientation,
	special = @special,
	coinValue = @coinValue,
	charges = @charges,
	attuneType = @attuneType,
	timeCreated = @timeCreated,
	whoCreated = @whoCreated
	
WHERE
playerID = @playerID
AND
wearingSlot = @wearingSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerWearing_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------
-- Select a single record from PlayerWearing
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerWearing_Select]
	@playerID int,
	@wearingSlot int
AS

SELECT	*
FROM	PlayerWearing
WHERE 	
playerID = @playerID
AND
wearingSlot = @wearingSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerWearing_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerWearing
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerWearing_Insert]
	@playerID int,
	@wearingSlot int,
	@itemID int,
	@attunedID int,
	@wearOrientation int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@attuneType bit,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
AS

INSERT PlayerWearing(playerID, wearingSlot,itemID, attunedID, wearOrientation, special, coinValue, charges, attuneType, timeCreated, whoCreated)
VALUES (@playerID, @wearingSlot, @itemID, @attunedID, @wearOrientation, @special, @coinValue, @charges, @attuneType, @timeCreated, @whoCreated)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerWearing_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerWearing
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerWearing_Delete]
	@playerID int
AS

DELETE	PlayerWearing
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSpells_Update]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerSpells
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSpells_Update]
	@PlayerID int,
	@SpellSlot int,
	@SpellID int,
	@ChantString nvarchar(255)=NULL
	
AS
UPDATE PlayerSpells
SET      
	SpellID = @SpellID,
	ChantString = @ChantString
	
WHERE
PlayerID = @PlayerID
AND
SpellSlot = @SpellSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSpells_Select]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------
-- Select a single record from PlayerSpells
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSpells_Select]
	@PlayerID int,
	@SpellSlot int
AS

SELECT	*
FROM	PlayerSpells
WHERE 	
PlayerID = @PlayerID
AND
SpellSlot = @SpellSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSpells_Insert]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerSpells
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSpells_Insert]
	@PlayerID int,
	@SpellSlot int,
	@SpellID int,
	@ChantString nvarchar(255)=NULL
AS

INSERT PlayerSpells(PlayerID, SpellSlot, SpellID, ChantString)
VALUES (@PlayerID, @SpellSlot, @SpellID, @ChantString)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSpells_Delete]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerSpells
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSpells_Delete]
	@playerID int
AS

DELETE	PlayerSpells
WHERE 	PlayerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSkills_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerSkills
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSkills_Update]

	@playerID int,
	@mace bigint = NULL,
	@bow bigint = NULL,
	@flail bigint = NULL,
	@dagger bigint = NULL,
	@rapier bigint = NULL,
	@twoHanded bigint = NULL,
	@staff bigint = NULL,
	@shuriken bigint = NULL,
	@sword  bigint = NULL,
	@threestaff bigint = NULL,
	@halberd bigint = NULL,
	@unarmed bigint = NULL,
	@thievery bigint = NULL,
	@magic bigint = NULL,
	@bash bigint = NULL,
	@highMace bigint = NULL,
	@highBow bigint = NULL,
	@highFlail bigint = NULL,
	@highDagger bigint = NULL,
	@highRapier bigint = NULL,
	@highTwohanded bigint = NULL,
	@highStaff bigint = NULL,
	@highShuriken bigint = NULL,
	@highSword  bigint = NULL,
	@highThreestaff bigint = NULL,
	@highHalberd bigint = NULL,
	@highUnarmed bigint = NULL,
	@highThievery bigint = NULL,
	@highMagic bigint = NULL,
	@highBash bigint = NULL,
	@trainedMace bigint = NULL,
	@trainedBow bigint = NULL,
	@trainedFlail bigint = NULL,
	@trainedDagger bigint = NULL,
	@trainedRapier bigint = NULL,
	@trainedTwohanded bigint = NULL,
	@trainedStaff bigint = NULL,
	@trainedShuriken bigint = NULL,
	@trainedSword  bigint = NULL,
	@trainedThreestaff bigint = NULL,
	@trainedHalberd bigint = NULL,
	@trainedUnarmed bigint = NULL,
	@trainedThievery bigint = NULL,
	@trainedMagic bigint = NULL,
	@trainedBash bigint = NULL
AS
UPDATE PlayerSkills
SET
	mace = @mace,
	bow = @bow,
	flail = @flail,
	dagger = @dagger,
	rapier = @rapier,
	twoHanded = @twohanded,
	staff = @staff,
	shuriken = @shuriken,
	sword = @sword,
	threestaff = @threestaff,
	halberd = @halberd,
	unarmed = @unarmed,
	thievery = @thievery,
	magic = @magic,
	bash = @bash,
	highMace = @highMace,
	highBow = @highBow,
	highFlail = @highFlail,
	highDagger = @highDagger,
	highRapier = @highRapier,
	highTwoHanded = @highTwoHanded,
	highStaff = @highStaff,
	highShuriken = @highShuriken,
	highSword = @highSword,
	highThreestaff = @highThreestaff,
	highHalberd = @highHalberd,
	highUnarmed = @highUnarmed,
	highThievery = @highThievery,
	highMagic = @highMagic,
	highBash = @highBash,
	trainedMace = @trainedMace,
	trainedBow = @trainedBow,
	trainedFlail = @trainedFlail,
	trainedDagger = @trainedDagger,
	trainedRapier = @trainedRapier,
	trainedTwoHanded = @trainedTwoHanded,
	trainedStaff = @trainedStaff,
	trainedShuriken = @trainedShuriken,
	trainedSword = @trainedSword,
	trainedThreestaff = @trainedThreestaff,
	trainedHalberd = @trainedHalberd,
	trainedUnarmed = @trainedUnarmed,
	trainedThievery = @trainedThievery,
	trainedMagic = @trainedMagic,
	trainedBash = @trainedBash
WHERE
playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSkills_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from PlayerSkills
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSkills_Select]
	@playerID int
AS

SELECT	*
FROM	PlayerSkills
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSkills_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerSkills
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSkills_Insert]
	@playerID int,
	@mace bigint,
	@bow bigint,
	@flail bigint,
	@dagger bigint,
	@rapier bigint,
	@twoHanded bigint,
	@staff bigint,
	@shuriken bigint,
	@sword  bigint,
	@threestaff bigint,
	@halberd bigint,
	@unarmed bigint,
	@thievery bigint,
	@magic bigint,
	@bash bigint,
	@highMace bigint,
	@highBow bigint,
	@highFlail bigint,
	@highDagger bigint,
	@highRapier bigint,
	@highTwoHanded bigint,
	@highStaff bigint,
	@highShuriken bigint,
	@highSword  bigint,
	@highThreestaff bigint,
	@highHalberd bigint,
	@highUnarmed bigint,
	@highThievery bigint,
	@highMagic bigint,
	@highBash bigint,
	@trainedMace bigint,
	@trainedBow bigint,
	@trainedFlail bigint,
	@trainedDagger bigint,
	@trainedRapier bigint,
	@trainedTwoHanded bigint,
	@trainedStaff bigint,
	@trainedShuriken bigint,
	@trainedSword  bigint,
	@trainedThreestaff bigint,
	@trainedHalberd bigint,
	@trainedUnarmed bigint,
	@trainedThievery bigint,
	@trainedMagic bigint,
	@trainedBash bigint	
AS

INSERT PlayerSkills(playerID, mace, bow, flail, dagger, rapier, twoHanded, staff, shuriken, sword, threestaff, halberd, unarmed, thievery, magic, bash, highMace, highBow, highFlail, highDagger, highRapier, highTwoHanded, highStaff, highShuriken, highSword, highThreestaff, highHalberd, highUnarmed, highThievery, highMagic, highBash, trainedMace, trainedBow, trainedFlail, trainedDagger, trainedRapier, trainedTwoHanded, trainedStaff, trainedShuriken, trainedSword, trainedThreestaff, trainedHalberd, trainedUnarmed, trainedThievery, trainedMagic, trainedBash)
VALUES (@playerID, @mace, @bow, @flail, @dagger, @rapier, @twoHanded, @staff, @shuriken, @sword, @threestaff, @halberd, @unarmed, @thievery, @magic, @bash, @highMace, @highBow, @highFlail, @highDagger, @highRapier, @highTwoHanded, @highStaff, @highShuriken, @highSword, @highThreestaff, @highHalberd, @highUnarmed, @highThievery, @highMagic, @highBash, @trainedMace, @trainedBow, @trainedFlail, @trainedDagger, @trainedRapier, @trainedTwoHanded, @trainedStaff, @trainedShuriken, @trainedSword, @trainedThreestaff, @trainedHalberd, @trainedUnarmed, @trainedThievery, @trainedMagic, @trainedBash)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSkills_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerSkills
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSkills_Delete]
	@playerID int
AS

DELETE	PlayerSkills
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSettings_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerSettings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSettings_Update]
	@playerID int,
----------------------------------------------------------------------------
	@anonymous bit,
	@echo bit,
	@filterProfanity int,
	@friendsList nvarchar(4000),
	@friendNotify bit,
	@ignoreList nvarchar(4000),
	@immortal bit,
	@invisible bit,
	@receiveGroupInvites bit,
	@receivePages bit,
	@receiveTells bit,
	@showStaffTitle bit,
	@macros nvarchar(4000),
	@displayCombatDamage bit
    AS
	UPDATE PlayerSettings
    SET
	anonymous = @anonymous,
	echo = @echo,
	filterProfanity = @filterProfanity,
	friendsList = @friendsList,
	friendNotify = @friendNotify,
	ignoreList = @ignoreList,
	immortal = @immortal,
	invisible = @invisible,
	receiveGroupInvites = @receiveGroupInvites,
	receivePages = @receivePages,
	receiveTells = @receiveTells,
	showStaffTitle = @showStaffTitle,
	macros = @macros,
	displayCombatDamage = @displayCombatDamage
    WHERE playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSettings_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from PlayerSettings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSettings_Select]
	@playerID int
AS

SELECT	*
FROM	PlayerSettings
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSettings_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerSettings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSettings_Insert]
	@playerID int,
	@anonymous bit,
	@echo bit,
	@filterProfanity int,
	@friendsList nvarchar(4000),
	@friendNotify bit,
	@ignoreList nvarchar(4000),
	@immortal bit,
	@invisible bit,
	@receiveGroupInvites bit,
	@receivePages bit,
	@receiveTells bit,
	@showStaffTitle bit,
	@macros nvarchar(4000),
	@displayCombatDamage bit
	
AS

INSERT PlayerSettings(playerID, anonymous, echo, filterProfanity, friendsList, friendNotify,
ignoreList, immortal, invisible, receiveGroupInvites, receivePages, receiveTells, showStaffTitle,
macros, displayCombatDamage)
VALUES (@playerID, @anonymous, @echo, @filterProfanity, @friendsList, @friendNotify,
@ignoreList, @immortal, @invisible, @receiveGroupInvites, @receivePages, @receiveTells, @showStaffTitle,
@macros, @displayCombatDamage)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSettings_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerSettings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSettings_Delete]
	@playerID int
AS

DELETE	PlayerSettings
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSack_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerSack
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSack_Update]
	@PlayerID int,
	@SackSlot int,
	@SackItem int,
	@Attuned int,
	@SackGold float,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit,
	@FigExp bigint,
	@TimeCreated datetime,
	@WhoCreated nvarchar(100)
	
AS
UPDATE PlayerSack
SET      
	SackItem = @SackItem,
	Attuned = @Attuned,
	SackGold = @SackGold,
	Special = @Special,
	CoinValue = @CoinValue,
	Charges = @Charges,
	Venom = @Venom,
	attuneType = @WillAttune,
	FigExp = @FigExp,
	TimeCreated = @TimeCreated,
	WhoCreated = @WhoCreated
	
WHERE
PlayerID = @PlayerID
AND
SackSlot = @SackSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSack_Select]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
---------------------------------------------------------------------------
-- Select a single record from PlayerSack
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSack_Select]
	@PlayerID int,
	@SackSlot int
AS

SELECT	*
FROM	PlayerSack
WHERE 	
PlayerID = @PlayerID
AND
SackSlot = @SackSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSack_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerSack
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSack_Insert]
	@PlayerID int,
	@SackSlot int,
	@SackItem int,
	@Attuned int,
	@SackGold float,
	@Special nvarchar(255),
	@CoinValue float,
	@Charges int,
	@Venom int,
	@WillAttune bit,
	@FigExp bigint,
	@TimeCreated datetime,
	@WhoCreated nvarchar(100)
AS

INSERT PlayerSack(PlayerID, SackSlot, SackItem, Attuned, SackGold, Special, CoinValue, Charges, Venom, attuneType, FigExp, TimeCreated, WhoCreated)
VALUES (@PlayerID, @SackSlot, @SackItem, @Attuned, @SackGold, @Special, @CoinValue, @Charges, @Venom, @WillAttune, @FigExp, @TimeCreated, @WhoCreated)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerSack_Delete]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerSack
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerSack_Delete]
	@playerID int
AS

DELETE	PlayerSack
WHERE 	PlayerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerRings_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerRings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerRings_Update]
	@playerID int,
	@ringFinger int,
	@itemID int,
	@attunedID int,
	@isRecall bit,
	@wasRecall bit,
	@recallLand smallint,
	@recallMap smallint,
	@recallX int,
	@recallY int,
	@recallZ int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@attuneType int,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
	
AS
UPDATE PlayerRings
SET      
	itemID = @itemID,
	attunedID = @attunedID,
	isRecall = @isRecall,
	wasRecall = @wasRecall,
	recallLand = @recallLand,
	recallMap = @recallMap,
	recallX = @recallX,
	recallY = @recallY,
	recallZ = @recallZ,
	special = @special,
	coinValue = @coinValue,
	charges = @charges,
	attuneType = @attuneType,
	timeCreated = @timeCreated,
	whoCreated = @whoCreated
	
WHERE
PlayerID = @PlayerID
AND
RingFinger = @RingFinger
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerRings_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------
-- Select a single record from PlayerRings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerRings_Select]
	@playerID int,
	@ringFinger  int
AS

SELECT	*
FROM	PlayerRings
WHERE 	
playerID = @playerID
AND
ringFinger = @ringFinger
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerRings_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerRings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerRings_Insert]
	@playerID int,
	@ringFinger int,
	@itemID int,
	@attunedID int,
	@isRecall bit,
	@wasRecall bit,
	@recallLand smallint,
	@recallMap smallint,
	@recallX int,
	@recallY int,
	@recallZ int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@attuneType int,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
AS

INSERT PlayerRings(playerID, ringFinger, itemID, attunedID,
isRecall, wasRecall, recallLand, recallMap, recallX, recallY, recallZ,
special, coinValue, charges, attuneType, timeCreated, whoCreated)
VALUES (@playerID, @ringFinger, @itemID, @attunedID,
@isRecall, @wasRecall, @recallLand, @recallMap, @recallX, @recallY, @recallZ,
@special, @coinValue, @charges, @attuneType, @timeCreated, @whoCreated)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerRings_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerRings
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerRings_Delete]
	@playerID int
AS

DELETE	PlayerRings
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerQuests_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record from PlayerQuests using PlayerID and QuestID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerQuests_Update]
	@playerID int=NULL,
	@questID int = NULL,
	@timesCompleted int = NULL,
	@startDate varchar(50) = NULL,
	@finishDate varchar(50) = NULL,
	@currentStep smallint = NULL
AS

UPDATE PlayerQuests

SET
timesCompleted = @timesCompleted,
startDate = @startDate,
finishDate = @finishDate,
currentStep = @currentStep

WHERE playerID = @playerID AND questID = @questID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerQuests_Select_Single]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from PlayerQuests by Player ID and QuestID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerQuests_Select_Single]
	@playerID int=NULL,
	@questID int
AS

SELECT	*
FROM	PlayerQuests
WHERE 	playerID = @playerID AND questID = @questID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerQuests_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from PlayerQuests by PlayerID
----------------------------------------------------------------------------
Create PROC [dbo].[prApp_PlayerQuests_Select]
	@playerID int = NULL
AS

SELECT	*
FROM	PlayerQuests
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerQuests_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record from PlayerQuests
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerQuests_Insert]
	@playerID int=NULL,
	@questID int = NULL,
	@timesCompleted int = NULL,
	@startDate varchar(50) = NULL,
	@finishDate varchar(50) = NULL,
	@currentStep smallint = NULL
AS

INSERT PlayerQuests (playerID, questID, timesCompleted, startDate, finishDate, currentStep)
VALUES (@playerID, @questID, @timesCompleted, @startDate, @finishDate, @currentStep)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerQuests_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete all record from PlayerQuests with @playerID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerQuests_Delete]
	@playerID int
AS

DELETE	PlayerQuests
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerLocker_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerLocker
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerLocker_Update]
	@playerID int,
	@lockerSlot int,
	@itemID int,
	@attunedID int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@attuneType int,
	@figExp bigint,
	@nocked bit,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
	
AS
UPDATE PlayerLocker
SET      
	itemID = @itemID,
	attunedID = @attunedID,
	special = @special,
	coinValue = @coinValue,
	charges = @charges,
	attuneType = @attuneType,
	figExp = @figExp,
	nocked = @nocked,
	timeCreated = @timeCreated,
	whoCreated = @whoCreated
	
WHERE
playerID = @playerID
AND
lockerSlot = @lockerSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerLocker_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---------------------------------------------------------------------------
-- Select a single record from PlayerLocker
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerLocker_Select]
	@playerID int,
	@lockerSlot int
AS

SELECT	*
FROM	PlayerLocker
WHERE 	
playerID = @playerID
AND
lockerSlot = @lockerSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerLocker_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerLocker
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerLocker_Insert]
	@playerID int,
	@lockerSlot int,
	@itemID int,
	@attunedID int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@attuneType int,
	@figExp bigint,
	@nocked bit,
	@timeCreated datetime,
	@whoCreated nvarchar(50)
AS

INSERT PlayerLocker(playerID, lockerSlot, itemID, attunedID, special, coinValue, charges,
attuneType, figExp, nocked, timeCreated, whoCreated)
VALUES (@playerID, @lockerSlot, @itemID, @attunedID, @special, @coinValue, @charges,
@attuneType, @figExp, @nocked, @timeCreated, @whoCreated)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerLocker_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerLocker
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerLocker_Delete]
	@playerID int
AS

DELETE	PlayerLocker
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerHeld_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerHeld
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerHeld_Update]
	@playerID int,
	@rightHand bit,
----------------------------------------------------------------------------
	@itemID int,
	@attunedID int,
	@special nvarchar(MAX),
	@charges int,
	@coinValue float,
	@figExp bigint,
	@venom int,
	@attuneType bit,
	@nocked bit,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
	
AS
UPDATE PlayerHeld
SET
	itemID = @itemID,
	attunedID = @attunedID,
	special = @special,
	charges = @charges,
	coinValue = @coinValue,
	figExp = @figExp,
	venom = @venom,
	attuneType = @attuneType,
	nocked = @nocked,
	timeCreated = @timeCreated,
	whoCreated = @whoCreated
	
WHERE
playerID = @PlayerID
AND
rightHand = @rightHand
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerHeld_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------
-- Select a single record from PlayerHeld
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerHeld_Select]
	@playerID int,
	@rightHand int
AS

SELECT	*
FROM	PlayerHeld
WHERE 	
playerID = @playerID
AND
rightHand = @rightHand
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerHeld_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerHeld
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerHeld_Insert]
	@playerID int,
	@rightHand int,
	@itemID int,
	@attunedID int,
	@special nvarchar(4000),
	@charges int,
	@coinValue float,
	@figExp bigint,
	@venom int,
	@attuneType bit,
	@nocked bit,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
AS

INSERT PlayerHeld(playerID, rightHand, itemID, attunedID, special, charges, coinValue,
figExp, venom, attuneType, nocked, timeCreated, whoCreated)
VALUES (@playerID, @rightHand, @itemID, @attunedID, @special, @charges, @coinValue,
@figExp, @venom, @attuneType, @nocked, @timeCreated, @whoCreated)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerHeld_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerHeld
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerHeld_Delete]
	@playerID int
AS

DELETE	PlayerHeld
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerFlags_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record from PlayerFlags using PlayerID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerFlags_Update]
	@playerID int = NULL,
	@questFlags nvarchar(MAX) = NULL,
	@contentFlags nvarchar(MAX) = NULL
AS

UPDATE PlayerFlags

SET
questFlags = @questFlags,
contentFlags = @contentFlags

WHERE playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerFlags_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select record from PlayerFlags by PlayerID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerFlags_Select]
	@playerID int = NULL
AS

SELECT	*
FROM	PlayerFlags
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerFlags_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerFlags
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerFlags_Insert]
	@playerID int = NULL,
	@questFlags nvarchar(MAX) = NULL,
	@contentFlags nvarchar(MAX) = NULL

AS

INSERT PlayerFlags (playerID, questFlags, contentFlags)
VALUES (@playerID, @questFlags, @contentFlags)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerFlags_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerFlags
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerFlags_Delete]
	@playerID int
AS

DELETE	PlayerFlags
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerEffects_Update]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerEffects
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerEffects_Update]
	@PlayerID int,
	@EffectSlot int,
	@EffectID int,
	@Amount int,
	@Duration int
	
AS
UPDATE PlayerEffects
SET      
	EffectID = @EffectID,
	Amount = @Amount,
	Duration = @Duration
	
WHERE
PlayerID = @PlayerID
AND
EffectSlot = @EffectSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerEffects_Select]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- Select a single record from PlayerEffects
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerEffects_Select]
	@PlayerID int,
	@EffectSlot int
AS

SELECT	*
FROM	PlayerEffects
WHERE 	
PlayerID = @PlayerID
AND
EffectSlot = @EffectSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerEffects_Insert]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerEffects
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerEffects_Insert]
	@PlayerID int,
	@EffectSlot int,
	@EffectID int,
	@Amount int,
	@Duration int
AS

INSERT PlayerEffects(PlayerID, EffectSlot, EffectID, Amount, Duration)
VALUES (@PlayerID, @EffectSlot, @EffectID, @Amount, @Duration)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerEffects_Delete]      ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerEffects
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerEffects_Delete]
	@playerID int
AS

DELETE	PlayerEffects
WHERE 	PlayerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerBelt_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record into PlayerBelt
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerBelt_Update]
	@playerID int,
	@beltSlot int,
	@itemID int,
	@attunedID int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@venom int,
	@attuneType int,
	@figExp bigint,
	@nocked bit,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
	
AS
UPDATE PlayerBelt
SET      
	itemID = @itemID,
	attunedID = @attunedID,
	special = @special,
	coinValue = @coinValue,
	charges = @charges,
	venom = @venom,
	attuneType = @attuneType,
	figExp = @figExp,
	nocked = @nocked,
	timeCreated = @timeCreated,
	whoCreated = @whoCreated
	
WHERE
playerID = @playerID
AND
beltSlot = @beltSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerBelt_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--------------------------------------------------------------------------
-- Select a single record from PlayerLocker
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerBelt_Select]
	@playerID int,
	@beltSlot int
AS

SELECT	*
FROM	PlayerBelt
WHERE 	
playerID = @playerID
AND
beltSlot = @beltSlot
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerBelt_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into PlayerBelt
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerBelt_Insert]
	@playerID int,
	@beltSlot int,
	@itemID int,
	@attunedID int,
	@special nvarchar(255),
	@coinValue float,
	@charges int,
	@venom int,
	@attuneType int,
	@figExp bigint,
	@nocked bit,
	@timeCreated datetime,
	@whoCreated nvarchar(100)
AS

INSERT PlayerBelt(playerID,beltSlot, itemID, attunedID, special, coinValue, charges,
venom, attuneType, figExp, nocked, timeCreated, whoCreated)
VALUES (@playerID, @beltSlot, @itemID, @attunedID, @special, @coinValue, @charges,
@venom, @attuneType, @figExp, @nocked, @timeCreated, @whoCreated)
GO
/****** Object:  StoredProcedure [dbo].[prApp_PlayerBelt_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from PlayerBelt
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_PlayerBelt_Delete]
	@playerID int
AS

DELETE	PlayerBelt
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Select_By_Name]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from Player by name
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Select_By_Name]
	@Name nvarchar(255)=NULL
AS

SELECT	*
FROM	Player
WHERE 	LOWER(Name) = LOWER(@Name)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Select_By_Account]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from Player on an Account
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Select_By_Account]
	@accountID  int
AS

SELECT	*
FROM	Player
WHERE 	accountID = @accountID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from Player
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Select]
	@playerID int
AS

SELECT	*
FROM	Player
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into Player
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Insert]
	@accountID int,
	@notes text = NULL,
	@account nvarchar(20)=NULL,
	@name varchar(50)=NULL,
	@gender smallint= NULL,
	@race varchar(50) = NULL,
	@classFullName varchar(50) = NULL,
	@classType varchar(50) = NULL,
	@visualKey varchar(50) = NULL,
	@alignment int= NULL,
	@confRoom int= NULL,
	@impLevel int= NULL,
	@ancestor bit,
	@ancestorID int= NULL,
	@land smallint= NULL,
	@map smallint= NULL,
	@xCord smallint= NULL,
	@yCord smallint= NULL,
	@zCord int = NULL,
	@dirPointer char(1)=NULL,
	@stunned smallint = NULL,
	@floating smallint = NULL,
	@dead bit,
	@fighterSpecialization varchar(50) = NULL,
	@level smallint = NULL,
	@exp bigint = NULL,
	@hits int = NULL,
	@hitsAdjustment int = NULL,
	@hitsMax int = NULL,
	@hitsDoctored int = NULL,
	@stamina int = NULL,
	@staminaAdjustment int = NULL,
	@stamLeft int = NULL,
	@mana int = NULL,
	@manaMax int = NULL,
	@manaAdjustment int = NULL,
	@age int = NULL,
	@roundsPlayed bigint = NULL,
	@numKills int = NULL,
	@numDeaths int = NULL,
	@bankGold float = NULL,
	@strength int = NULL,
	@dexterity int = NULL,
	@intelligence int = NULL,
	@wisdom int = NULL,
	@constitution int = NULL,
	@charisma int = NULL,
	@strengthAdd int = NULL,
	@dexterityAdd int = NULL,
	@birthday datetime = NULL,
	@lastOnline datetime = NULL,
	@UW_hitsMax int = NULL,
	@UW_hitsAdjustment int = NULL,
	@UW_staminaMax int = NULL,
	@UW_staminaAdjustment int = NULL,
	@UW_manaMax int = NULL,
	@UW_manaAdjustment int = NULL,
	@UW_intestines bit,
	@UW_liver bit,
	@UW_lungs bit,
	@UW_stomach bit,
	@currentKarma int = NULL,
	@lifetimeKarma bigint = NULL,
	@lifetimeMarks int = NULL,
	@pvpDeaths bigint = NULL,
	@pvpKills bigint = NULL,
	@playersFlagged nvarchar(4000) = NULL,
	@playersKilled nvarchar(1000) = NULL
	
AS

INSERT Player( accountID, notes, account, name, gender, race, classFullName, classType,
visualKey,
alignment, confRoom, impLevel, ancestor, ancestorID,
land, map, xCord, yCord, zCord,
dirPointer, stunned, floating, dead, fighterSpecialization, [level],
[exp], hits, hitsMax, hitsAdjustment, hitsDoctored,
stamina, stamLeft, staminaAdjustment, mana, manaMax, manaAdjustment,
age, roundsPlayed, numKills, numDeaths, bankGold,
strength, dexterity, intelligence, wisdom, constitution, charisma, strengthAdd, dexterityAdd,
birthday, lastOnline,
UW_hitsMax, UW_hitsAdjustment, UW_staminaMax, UW_staminaAdjustment, UW_manaMax, UW_manaAdjustment,
UW_intestines, UW_liver, UW_lungs, UW_stomach, currentKarma, lifetimeKarma, pvpDeaths, pvpKills, playersFlagged, playersKilled)

VALUES (@accountID, @notes, @account, @name, @gender, @race, @classFullName, @classType,
@visualKey,
@alignment, @confRoom, @impLevel, COALESCE(@ancestor, 0), @ancestorID,
@land, @map, @XCord, @yCord, @zCord,
@dirPointer, @stunned, @floating, COALESCE(@dead, 0), @fighterSpecialization, @level,
@exp, @hits, @hitsMax, @hitsAdjustment, @hitsDoctored,
@stamina, @stamLeft, @staminaAdjustment, @mana, @manaMax, @manaAdjustment,
@age, @roundsPlayed, @numKills, @numDeaths, @bankGold,
@strength, @dexterity, @intelligence, @wisdom, @constitution, @charisma, @strengthAdd, @dexterityAdd,
@birthday, @lastOnline,
@UW_hitsMax, @UW_hitsAdjustment, @UW_staminaMax, @UW_staminaAdjustment, @UW_manaMax, @UW_manaAdjustment,
COALESCE(@UW_intestines, 0), COALESCE(@UW_liver, 0), COALESCE(@UW_lungs, 0), COALESCE(@UW_stomach, 0), @currentKarma, @lifetimeKarma, @pvpDeaths, @pvpKills, @playersFlagged, @playersKilled)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from Player
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Player_Delete]
	@playerID int
AS

DELETE	Player
WHERE 	playerID = @playerID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Player_Check]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prApp_Player_Check] 
	@name nvarchar (20) =NULL
AS

SELECT	*
FROM		Player
WHERE	[name] = @name
GO
/****** Object:  StoredProcedure [dbo].[prApp_NPCLocation_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_NPCLocation_Update] 
	-- Add the parameters for the stored procedure here
	@NPCIndex int, 
	@active int,
	@name nvarchar(255),
	@facet smallint,
	@land smallint,
	@map smallint,
	@xCord smallint,
	@yCord smallint,
	@zCord int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE NPCLocation
	SET name=@name, active=@active, xCord=@xCord, yCord=@yCord, zCord=@zCord
	WHERE NPCIndex=@NPCIndex AND facet=@facet AND land=@land AND map=@map; 
	IF @@ROWCOUNT=0
		INSERT INTO NPCLocation (NPCIndex, active, name, facet, land, map, xCord, yCord, zCord)
		VALUES (@NPCIndex, @active, @name, @facet, @land, @map, @xCord, @yCord, @zCord)
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_NPCLocation_Clear]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Clear all entries from the NPCLocation table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_NPCLocation_Clear]

AS

DELETE	NPCLocation
GO
/****** Object:  StoredProcedure [dbo].[prApp_NPCLocation_By_MapID]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_NPCLocation_By_MapID] 
	-- Add the parameters for the stored procedure here
	@facet smallint,
	@land smallint,
	@map smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM NPCLocation
	WHERE facet=@facet AND land=@land AND map=@map 
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_NPC_Select_All]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from NPC
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_NPC_Select_All]
AS

SELECT	*
FROM	NPC
GO
/****** Object:  StoredProcedure [dbo].[prApp_NPC_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from NPC
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_NPC_Select]
	@npcID int
AS

SELECT	*
FROM	NPC
WHERE 	npcID = @npcID
GO
/****** Object:  StoredProcedure [dbo].[prApp_NPC_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Delete a single record from NPC
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_NPC_Delete]
	@NpcID int
AS

DELETE	NPC
WHERE 	NpcID = @NpcID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Map_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record in Map
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Map_Update]
	@MapID int,
	@LandID int,
	@Name nvarchar(510) = NULL,
	@ShortDesc nvarchar(510) = NULL,
	@LongDesc nvarchar(510) = NULL
AS

UPDATE	Map
SET	LandID = @LandID,
	Name = @Name,
	ShortDesc = @ShortDesc,
	LongDesc = @LongDesc
WHERE 	MapID = @MapID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Map_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a all records from Map with landID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Map_Select]
	@landID smallint
AS

SELECT	*
FROM	Map
WHERE 	landID = @landID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Map_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into Map
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Map_Insert]
	@LandID int,
	@Name nvarchar(510) = NULL,
	@ShortDesc nvarchar(510) = NULL,
	@LongDesc nvarchar(510) = NULL
AS

INSERT Map(LandID, Name, ShortDesc, LongDesc)
VALUES (@LandID, @Name, @ShortDesc, @LongDesc)

RETURN SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[prApp_Map_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
----------------------------------------------------------------------------
-- Delete a single record from Map
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Map_Delete]
	@MapID int
AS

DELETE	Map
WHERE 	MapID = @MapID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Mail_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record in Mail
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Mail_Update]
	@mailID bigint,
	@attachment bit,
	@readByReceiver bit
AS

UPDATE	Mail
SET	
	attachment = @attachment,
	readByReceiver = @readByReceiver

WHERE (mailID = @mailID)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Mail_Select_By_SenderID]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a all records from Map with landID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Mail_Select_By_SenderID]
	@senderID int
AS

SELECT	*
FROM	Mail
WHERE 	senderID = @senderID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Mail_Select_By_ReceiverID]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a all records from Map with landID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Mail_Select_By_ReceiverID]
	@receiverID int
AS

SELECT	*
FROM	Mail
WHERE 	receiverID = @receiverID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Mail_Select_By_MailID]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a all records from Map with landID
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Mail_Select_By_MailID]
	@mailID int
AS

SELECT	*
FROM	Mail
WHERE 	mailID = @mailID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Mail_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into Mail
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Mail_Insert]
	@mailID bigint,
	@senderID int,
	@receiverID int,
	@timeSent datetime,
	@subject nvarchar(50) = NULL,
	@body varchar(MAX) = NULL,
	@attachment bit,
	@readByReceiver bit
AS

INSERT Mail(mailID, senderID, receiverID, timeSent, subject, body, attachment, readByReceiver)
VALUES (@mailID, @senderID, @receiverID, @timeSent, @subject, @body, @attachment, @readByReceiver)

RETURN SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[prApp_Mail_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from Mail
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Mail_Delete]
	@mailID int
AS

DELETE	Mail
WHERE 	mailID = @mailID
GO
/****** Object:  StoredProcedure [dbo].[prApp_LivePC_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LivePC_Update] 
	@uniqueId int,
	@name nvarchar(255),
	@facet smallint,
	@map smallint,
	@xCord smallint,
	@yCord smallint,
	@zCord int,
	@level int,
	@hits int,
	@fullHits int,
	@mana int,
	@fullMana int,
	@lastCommand nvarchar(255),
	@lastActiveRound int,
	@isDead bit,
	@mostHatedId int=NULL,
	@hateCenterX smallint=NULL,
	@hateCenterY smallint=NULL,
	@loveCenterX smallint=NULL,
	@loveCenterY smallint=NULL,
	@fearLove int=NULL,
	@NpcTypeCode 		int=NULL,
	@BaseTypeCode 		int=NULL,
	@CharacterClassCode 	int=NULL,
	@AlignCode 		int=NULL,
	@numAttackers int=NULL,
	@lairLocationX smallint=NULL,
	@lairLocationY smallint=NULL,
	@lairLocationZ int=NULL,
	@priorityCode int=NULL,
	@effects nvarchar(255)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE LivePC
	SET uniqueId=@uniqueId,
	name=@name,
	facet=@facet,
	map=@map,
	xCord=@xCord,
	yCord=@yCord,
	zCord=@zCord,
	level=@level,
	hits=@hits,
	fullHits=@fullHits,
	mana=@mana,
	fullMana=@fullMana,
	lastCommand=@lastCommand,
	lastActiveRound=@lastActiveRound,
	isDead=@isDead,
	effects=@effects
	WHERE uniqueId=@uniqueId; 
	IF @@ROWCOUNT=0
		INSERT INTO LivePC (
			uniqueId,
			name,
			facet,
			map,
			xCord,
			yCord,
			zCord,
			level,
			hits,
			fullHits,
			mana,
			fullMana,
			lastCommand,
			lastActiveRound,
			firstActiveRound,
			isDead,
			effects
		)
		VALUES (
			@uniqueId,
			@name,
			@facet,
			@map,
			@xCord,
			@yCord,
			@zCord,
			@level,
			@hits,
			@fullHits,
			@mana,
			@fullMana,
			@lastCommand,
			@lastActiveRound,
			@lastActiveRound,
			@isDead,
			@effects
		)
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_LivePC_Clear]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Clear all entries from the LivePC table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_LivePC_Clear]

AS

DELETE	LivePC
GO
/****** Object:  StoredProcedure [dbo].[prApp_LivePC_By_MapID]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LivePC_By_MapID] 
	-- Add the parameters for the stored procedure here
	@facet smallint,
	@map smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM LivePC
	WHERE facet=@facet AND map=@map 
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LiveNPC_Update] 
	@uniqueId int,
	@name nvarchar(255),
	@facet smallint,
	@map smallint,
	@xCord smallint,
	@yCord smallint,
	@zCord int,
	@level int,
	@hits int,
	@fullHits int,
	@mana int,
	@fullMana int,
	@lastCommand nvarchar(255),
	@lastActiveRound int,
	@isDead bit,
	@mostHatedId int=NULL,
	@hateCenterX smallint=NULL,
	@hateCenterY smallint=NULL,
	@loveCenterX smallint=NULL,
	@loveCenterY smallint=NULL,
	@fearLove int=NULL,
	@NpcTypeCode 		int=NULL,
	@BaseTypeCode 		int=NULL,
	@CharacterClassCode 	int=NULL,
	@AlignCode 		int=NULL,
	@numAttackers int=NULL,
	@lairLocationX smallint=NULL,
	@lairLocationY smallint=NULL,
	@lairLocationZ int=NULL,
	@priorityCode int=NULL,
	@effects nvarchar(255)=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE LiveNPC
	SET uniqueId=@uniqueId,
	name=@name,
	facet=@facet,
	map=@map,
	xCord=@xCord,
	yCord=@yCord,
	zCord=@zCord,
	level=@level,
	hits=@hits,
	fullHits=@fullHits,
	mana=@mana,
	fullMana=@fullMana,
	lastCommand=@lastCommand,
	lastActiveRound=@lastActiveRound,
	isDead=@isDead,
	mostHatedId=@mostHatedId,
	hateCenterX=@hateCenterX,
	hateCenterY=@hateCenterY,
	loveCenterX=@loveCenterX,
	loveCenterY=@loveCenterY,
	fearLove=@fearLove,
	NpcTypeCode=@NpcTypeCode,
	BaseTypeCode=@BaseTypeCode,
	CharacterClassCode=@CharacterClassCode,
	AlignCode=@AlignCode,
	numAttackers=@numAttackers,
	lairLocationX=@lairLocationX,
	lairLocationY=@lairLocationY,
	lairLocationZ=@lairLocationZ,
	priorityCode=@priorityCode,
	effects=@effects
	WHERE uniqueId=@uniqueId; 
	IF @@ROWCOUNT=0
		INSERT INTO LiveNPC (
			uniqueId,
			name,
			facet,
			map,
			xCord,
			yCord,
			zCord,
			level,
			hits,
			fullHits,
			mana,
			fullMana,
			lastCommand,
			lastActiveRound,
			firstActiveRound,
			isDead,
			mostHatedId,
			hateCenterX,
			hateCenterY,
			loveCenterX,
			loveCenterY,
			fearLove,
			NpcTypeCode,
			BaseTypeCode,
			CharacterClassCode,
			AlignCode,
			numAttackers,
			lairLocationX,
			lairLocationY,
			lairLocationZ,
			priorityCode,
			effects
		)
		VALUES (
			@uniqueId,
			@name,
			@facet,
			@map,
			@xCord,
			@yCord,
			@zCord,
			@level,
			@hits,
			@fullHits,
			@mana,
			@fullMana,
			@lastCommand,
			@lastActiveRound,
			@lastActiveRound,
			@isDead,
			@mostHatedId,
			@hateCenterX,
			@hateCenterY,
			@loveCenterX,
			@loveCenterY,
			@fearLove,
			@NpcTypeCode,
			@BaseTypeCode,
			@CharacterClassCode,
			@AlignCode,
			@numAttackers,
			@lairLocationX,
			@lairLocationY,
			@lairLocationZ,
			@priorityCode,
			@effects
		)
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_Clear]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Clear all entries from the LiveNPC table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_LiveNPC_Clear]

AS

DELETE	LiveNPC
GO
/****** Object:  StoredProcedure [dbo].[prApp_LiveNPC_By_MapID]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LiveNPC_By_MapID] 
	-- Add the parameters for the stored procedure here
	@facet smallint,
	@map smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM LiveNPC
	WHERE facet=@facet AND map=@map 
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_LiveCell_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LiveCell_Update] 
	@lastRoundChanged int,
	@facet smallint,
	@map smallint,
	@xCord smallint,
	@yCord smallint,
	@zCord int,
	@cellGraphic char(2),
	@displayGraphic char(2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    UPDATE LiveCell
	SET lastRoundChanged=@lastRoundChanged,
	facet=@facet,
	map=@map,
	xCord=@xCord,
	yCord=@yCord,
	zCord=@zCord,
	cellGraphic=@cellGraphic,
	displayGraphic=@displayGraphic
	WHERE facet=@facet AND map=@map AND xCord=@xCord AND yCord=@yCord AND zCord=@zCord; 
	IF @@ROWCOUNT=0
		INSERT INTO LiveCell (
			lastRoundChanged,
			facet,
			map,
			xCord,
			yCord,
			zCord,
			cellGraphic,
			displayGraphic
		)
		VALUES (
			@lastRoundChanged,
			@facet,
			@map,
			@xCord,
			@yCord,
			@zCord,
			@cellGraphic,
			@displayGraphic
		)
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_LiveCell_Clear]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Clear all entries from the LiveCell table
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_LiveCell_Clear]

AS

DELETE	LiveCell
GO
/****** Object:  StoredProcedure [dbo].[prApp_LiveCell_By_MapID]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joe
-- Create date: 
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[prApp_LiveCell_By_MapID] 
	-- Add the parameters for the stored procedure here
	@facet smallint,
	@map smallint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT *
	FROM LiveCell
	WHERE facet=@facet AND map=@map 
END
GO
/****** Object:  StoredProcedure [dbo].[prApp_Land_Update]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Update a single record in Land
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Land_Update]
	@landID int,
	@name varchar(50),
	@shortDesc varchar(255) = NULL,
	@longDesc text = NULL
AS

UPDATE	Land
SET	
	[name] = @name,
	shortDesc = @shortDesc,
	longDesc = @longDesc

WHERE (landID = @landID)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Land_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from Land
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Land_Select]

AS

SELECT	*
FROM	Land
GO
/****** Object:  StoredProcedure [dbo].[prApp_Land_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into Land
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Land_Insert]
	@name varchar(50),
	@shortDesc varchar(255) = NULL,
	@longDesc text = NULL
AS

INSERT Land(name, shortDesc, longDesc)
VALUES (@name, @shortDesc, @longDesc)

RETURN SCOPE_IDENTITY()
GO
/****** Object:  StoredProcedure [dbo].[prApp_Land_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from Land
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Land_Delete]
	@landID int
AS

DELETE	Land
WHERE 	landID = @landID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Facet_Select_All]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from Facet
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Facet_Select_All]
AS
SELECT	*
FROM	Facet
GO
/****** Object:  StoredProcedure [dbo].[prApp_CharGen_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from CharGen
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_CharGen_Select]
	@race varchar(20),
	@classType int

AS

SELECT	*
FROM	CharGen
WHERE 	(race = @race) AND (classType = @classType)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Cell_Select_By_Map]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from Cell by map  name
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Cell_Select_By_Map]
	@mapName nvarchar(50)

AS

SELECT	*
FROM	Cell
WHERE mapName = @mapName
GO
/****** Object:  StoredProcedure [dbo].[prApp_Cell_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from CellsInfo by MapName, XCord, YCord
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Cell_Select]
	@mapName nvarchar(50),
	@xCord int,
	@yCord int
AS

SELECT	*
FROM	Cell
WHERE (mapName = @mapName AND xCord = @xCord AND yCord = @yCord)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Cell_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from Cell
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Cell_Delete]
	@mapName nvarchar(50),
	@xCord int,
	@yCord int
AS

DELETE	Cell
WHERE 	mapName = @mapName AND xCord = @xCord AND yCord = @yCord
GO
/****** Object:  StoredProcedure [dbo].[prApp_CatalogItem_Select_All]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from CatalogItem
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_CatalogItem_Select_All]
AS

SELECT	*
FROM	CatalogItem
GO
/****** Object:  StoredProcedure [dbo].[prApp_CatalogItem_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from CatalogItem
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_CatalogItem_Select]
	@catalogID int
AS

SELECT	*
FROM	CatalogItem
WHERE 	catalogID = @catalogID
GO
/****** Object:  StoredProcedure [dbo].[prApp_BannedIP_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from BannedIP
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_BannedIP_Select]
AS
SELECT	*
FROM	BannedIP
GO
/****** Object:  StoredProcedure [dbo].[prApp_BannedIP_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into BannedIP
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_BannedIP_Insert]
	@bannedIP varchar(50),
	@dateBanned datetime,
	@reason varchar(MAX) = NULL
	
AS

INSERT BannedIP(bannedIP, dateBanned, reason)
VALUES (@bannedIP, @dateBanned, @reason)
GO
/****** Object:  StoredProcedure [dbo].[prApp_BannedIP_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from BannedIP
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_BannedIP_Delete]
	@bannedIP int
AS

DELETE	BannedIP
WHERE 	bannedIP = @bannedIP
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Select_All]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select all records from Account
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Account_Select_All]
AS

SELECT	*
FROM	Account
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Select]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Select a single record from Account
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Account_Select]
	@account  nvarchar (20)=NULL
AS

SELECT	*
FROM	Account
WHERE 	account = @account
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Insert]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Insert a single record into Account
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Account_Insert]
	@account  nvarchar(20),
	@password nvarchar(20),
	@ipAddress nvarchar(16),
	@ipAddressList nvarchar(1160),
	@email nvarchar(50)
AS

INSERT Account(account, password, ipAddress, ipAddressList, email)
VALUES (@account, @password, @ipAddress, @ipAddressList, @email)
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Delete]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----------------------------------------------------------------------------
-- Delete a single record from Account
----------------------------------------------------------------------------
CREATE PROC [dbo].[prApp_Account_Delete]
	@accountID int
AS

DELETE	Account
WHERE 	accountID = @accountID
GO
/****** Object:  StoredProcedure [dbo].[prApp_Account_Check]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prApp_Account_Check] 
	@account nvarchar (20) =NULL
AS

SELECT	*
FROM		Account
WHERE	account = @account
GO
/****** Object:  UserDefinedFunction [dbo].[fnTableColumnInfo]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE       FUNCTION [dbo].[fnTableColumnInfo](@sTableName varchar(128))
RETURNS TABLE
AS
	RETURN
	SELECT	c.name AS sColumnName,
		c.colid AS nColumnID,
		dbo.fnIsColumnPrimaryKey(@sTableName, c.name) AS bPrimaryKeyColumn,
		CASE 	WHEN t.name IN ('char', 'varchar', 'binary', 'varbinary', 'nchar', 'nvarchar') THEN 1
			WHEN t.name IN ('decimal', 'numeric') THEN 2
			ELSE 0
		END AS nAlternateType,
		c.length AS nColumnLength,
		c.prec AS nColumnPrecision,
		c.scale AS nColumnScale, 
		c.IsNullable, 
		SIGN(c.status & 128) AS IsIdentity,
		t.name as sTypeName,
		dbo.fnColumnDefault(@sTableName, c.name) AS sDefaultValue
	FROM	syscolumns c 
		INNER JOIN systypes t ON c.xtype = t.xtype and c.usertype = t.usertype
	WHERE	c.id = OBJECT_ID(@sTableName)
GO
/****** Object:  StoredProcedure [dbo].[pr__SYS_MakeUpdateRecordProc]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE         PROC [dbo].[pr__SYS_MakeUpdateRecordProc]
	@sTableName varchar(128),
	@bExecute bit = 0
AS

IF dbo.fnTableHasPrimaryKey(@sTableName) = 0
 BEGIN
	RAISERROR ('Procedure cannot be created on a table with no primary key.', 10, 1)
	RETURN
 END

DECLARE	@sProcText varchar(8000),
	@sKeyFields varchar(2000),
	@sSetClause varchar(2000),
	@sWhereClause varchar(2000),
	@sColumnName varchar(128),
	@nColumnID smallint,
	@bPrimaryKeyColumn bit,
	@nAlternateType int,
	@nColumnLength int,
	@nColumnPrecision int,
	@nColumnScale int,
	@IsNullable bit, 
	@IsIdentity int,
	@sTypeName varchar(128),
	@sDefaultValue varchar(4000),
	@sCRLF char(2),
	@sTAB char(1)

SET	@sTAB = char(9)
SET 	@sCRLF = char(13) + char(10)

SET 	@sProcText = ''
SET 	@sKeyFields = ''
SET	@sSetClause = ''
SET	@sWhereClause = ''

SET 	@sProcText = @sProcText + 'IF EXISTS(SELECT * FROM sysobjects WHERE name = ''prApp_' + @sTableName + '_Update'')' + @sCRLF
SET 	@sProcText = @sProcText + @sTAB + 'DROP PROC prApp_' + @sTableName + '_Update' + @sCRLF
IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF

SET 	@sProcText = @sProcText + @sCRLF

PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)

SET 	@sProcText = ''
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + '-- Update a single record in ' + @sTableName + @sCRLF
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + 'CREATE PROC prApp_' + @sTableName + '_Update' + @sCRLF

DECLARE crKeyFields cursor for
	SELECT	*
	FROM	dbo.fnTableColumnInfo(@sTableName)
	ORDER BY 2

OPEN crKeyFields


FETCH 	NEXT 
FROM 	crKeyFields 
INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
	@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
	@IsIdentity, @sTypeName, @sDefaultValue
				
WHILE (@@FETCH_STATUS = 0)
 BEGIN
	IF (@sKeyFields <> '')
		SET @sKeyFields = @sKeyFields + ',' + @sCRLF 

	SET @sKeyFields = @sKeyFields + @sTAB + '@' + @sColumnName + ' ' + @sTypeName

	IF (@nAlternateType = 2) --decimal, numeric
		SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnPrecision AS varchar(3)) + ', ' 
				+ CAST(@nColumnScale AS varchar(3)) + ')'

	ELSE IF (@nAlternateType = 1) --character and binary
		SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnLength AS varchar(4)) +  ')'

	IF (@bPrimaryKeyColumn = 1)
	 BEGIN
		IF (@sWhereClause = '')
			SET @sWhereClause = @sWhereClause + 'WHERE ' 
		ELSE
			SET @sWhereClause = @sWhereClause + ' AND ' 

		SET @sWhereClause = @sWhereClause + @sTAB + @sColumnName  + ' = @' + @sColumnName + @sCRLF 
	 END
	ELSE
		IF (@IsIdentity = 0)
		 BEGIN
			IF (@sSetClause = '')
				SET @sSetClause = @sSetClause + 'SET'
			ELSE
				SET @sSetClause = @sSetClause + ',' + @sCRLF 
			SET @sSetClause = @sSetClause + @sTAB + @sColumnName  + ' = '
			IF (@sTypeName = 'timestamp')
				SET @sSetClause = @sSetClause + 'NULL'
			ELSE IF (@sDefaultValue IS NOT NULL)
				SET @sSetClause = @sSetClause + 'COALESCE(@' + @sColumnName + ', ' + @sDefaultValue + ')'
			ELSE
				SET @sSetClause = @sSetClause + '@' + @sColumnName 
		 END

	IF (@IsIdentity = 0)
	 BEGIN
		IF (@IsNullable = 1) OR (@sTypeName = 'timestamp')
			SET @sKeyFields = @sKeyFields + ' = NULL'
	 END

	FETCH 	NEXT 
	FROM 	crKeyFields 
	INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
		@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
		@IsIdentity, @sTypeName, @sDefaultValue
 END

CLOSE crKeyFields
DEALLOCATE crKeyFields

SET 	@sSetClause = @sSetClause + @sCRLF

SET 	@sProcText = @sProcText + @sKeyFields + @sCRLF
SET 	@sProcText = @sProcText + 'AS' + @sCRLF
SET 	@sProcText = @sProcText + @sCRLF
SET 	@sProcText = @sProcText + 'UPDATE	' + @sTableName + @sCRLF
SET 	@sProcText = @sProcText + @sSetClause
SET 	@sProcText = @sProcText + @sWhereClause
SET 	@sProcText = @sProcText + @sCRLF
IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF


PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)
GO
/****** Object:  StoredProcedure [dbo].[pr__SYS_MakeSelectRecordProc]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE     PROC [dbo].[pr__SYS_MakeSelectRecordProc]
	@sTableName varchar(128),
	@bExecute bit = 0
AS

IF dbo.fnTableHasPrimaryKey(@sTableName) = 0
 BEGIN
	RAISERROR ('Procedure cannot be created on a table with no primary key.', 10, 1)
	RETURN
 END

DECLARE	@sProcText varchar(8000),
	@sKeyFields varchar(2000),
	@sSelectClause varchar(2000),
	@sWhereClause varchar(2000),
	@sColumnName varchar(128),
	@nColumnID smallint,
	@bPrimaryKeyColumn bit,
	@nAlternateType int,
	@nColumnLength int,
	@nColumnPrecision int,
	@nColumnScale int,
	@IsNullable bit, 
	@IsIdentity int,
	@sTypeName varchar(128),
	@sDefaultValue varchar(4000),
	@sCRLF char(2),
	@sTAB char(1)

SET	@sTAB = char(9)
SET 	@sCRLF = char(13) + char(10)

SET 	@sProcText = ''
SET 	@sKeyFields = ''
SET	@sSelectClause = ''
SET	@sWhereClause = ''

SET 	@sProcText = @sProcText + 'IF EXISTS(SELECT * FROM sysobjects WHERE name = ''prApp_' + @sTableName + '_Select'')' + @sCRLF
SET 	@sProcText = @sProcText + @sTAB + 'DROP PROC prApp_' + @sTableName + '_Select' + @sCRLF
IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF

SET 	@sProcText = @sProcText + @sCRLF

PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)

SET 	@sProcText = ''
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + '-- Select a single record from ' + @sTableName + @sCRLF
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + 'CREATE PROC prApp_' + @sTableName + '_Select' + @sCRLF

DECLARE crKeyFields cursor for
	SELECT	*
	FROM	dbo.fnTableColumnInfo(@sTableName)
	ORDER BY 2

OPEN crKeyFields

FETCH 	NEXT 
FROM 	crKeyFields 
INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
	@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
	@IsIdentity, @sTypeName, @sDefaultValue
				
WHILE (@@FETCH_STATUS = 0)
 BEGIN
	IF (@bPrimaryKeyColumn = 1)
	 BEGIN
		IF (@sKeyFields <> '')
			SET @sKeyFields = @sKeyFields + ',' + @sCRLF 
	
		SET @sKeyFields = @sKeyFields + @sTAB + '@' + @sColumnName + ' ' + @sTypeName
	
		IF (@nAlternateType = 2) --decimal, numeric
			SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnPrecision AS varchar(3)) + ', ' 
					+ CAST(@nColumnScale AS varchar(3)) + ')'
	
		ELSE IF (@nAlternateType = 1) --character and binary
			SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnLength AS varchar(4)) +  ')'

		IF (@sWhereClause = '')
			SET @sWhereClause = @sWhereClause + 'WHERE ' 
		ELSE
			SET @sWhereClause = @sWhereClause + ' AND ' 

		SET @sWhereClause = @sWhereClause + @sTAB + @sColumnName  + ' = @' + @sColumnName + @sCRLF 
	 END

	IF (@sSelectClause = '')
		SET @sSelectClause = @sSelectClause + 'SELECT'
	ELSE
		SET @sSelectClause = @sSelectClause + ',' + @sCRLF 

	SET @sSelectClause = @sSelectClause + @sTAB + @sColumnName 

	FETCH 	NEXT 
	FROM 	crKeyFields 
	INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
		@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
		@IsIdentity, @sTypeName, @sDefaultValue
 END

CLOSE crKeyFields
DEALLOCATE crKeyFields

SET 	@sSelectClause = @sSelectClause + @sCRLF

SET 	@sProcText = @sProcText + @sKeyFields + @sCRLF
SET 	@sProcText = @sProcText + 'AS' + @sCRLF
SET 	@sProcText = @sProcText + @sCRLF
SET 	@sProcText = @sProcText + @sSelectClause
SET 	@sProcText = @sProcText + 'FROM	' + @sTableName + @sCRLF
SET 	@sProcText = @sProcText + @sWhereClause
SET 	@sProcText = @sProcText + @sCRLF
IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF


PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)
GO
/****** Object:  StoredProcedure [dbo].[pr__SYS_MakeInsertRecordProc]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE       PROC [dbo].[pr__SYS_MakeInsertRecordProc]
	@sTableName varchar(128),
	@bExecute bit = 0
AS

IF dbo.fnTableHasPrimaryKey(@sTableName) = 0
 BEGIN
	RAISERROR ('Procedure cannot be created on a table with no primary key.', 10, 1)
	RETURN
 END

DECLARE	@sProcText varchar(8000),
	@sKeyFields varchar(2000),
	@sAllFields varchar(2000),
	@sAllParams varchar(2000),
	@sWhereClause varchar(2000),
	@sColumnName varchar(128),
	@nColumnID smallint,
	@bPrimaryKeyColumn bit,
	@nAlternateType int,
	@nColumnLength int,
	@nColumnPrecision int,
	@nColumnScale int,
	@IsNullable bit, 
	@IsIdentity int,
	@HasIdentity int,
	@sTypeName varchar(128),
	@sDefaultValue varchar(4000),
	@sCRLF char(2),
	@sTAB char(1)

SET 	@HasIdentity = 0
SET	@sTAB = char(9)
SET 	@sCRLF = char(13) + char(10)
SET 	@sProcText = ''
SET 	@sKeyFields = ''
SET	@sAllFields = ''
SET	@sWhereClause = ''
SET	@sAllParams  = ''

SET 	@sProcText = @sProcText + 'IF EXISTS(SELECT * FROM sysobjects WHERE name = ''prApp_' + @sTableName + '_Insert'')' + @sCRLF
SET 	@sProcText = @sProcText + @sTAB + 'DROP PROC prApp_' + @sTableName + '_Insert' + @sCRLF
IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF

SET 	@sProcText = @sProcText + @sCRLF

PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)

SET 	@sProcText = ''
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + '-- Insert a single record into ' + @sTableName + @sCRLF
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + 'CREATE PROC prApp_' + @sTableName + '_Insert' + @sCRLF

DECLARE crKeyFields cursor for
	SELECT	*
	FROM	dbo.fnTableColumnInfo(@sTableName)
	ORDER BY 2

OPEN crKeyFields


FETCH 	NEXT 
FROM 	crKeyFields 
INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
	@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
	@IsIdentity, @sTypeName, @sDefaultValue
				
WHILE (@@FETCH_STATUS = 0)
 BEGIN
	IF (@IsIdentity = 0)
	 BEGIN
		IF (@sKeyFields <> '')
			SET @sKeyFields = @sKeyFields + ',' + @sCRLF 

		SET @sKeyFields = @sKeyFields + @sTAB + '@' + @sColumnName + ' ' + @sTypeName

		IF (@sAllFields <> '')
		 BEGIN
			SET @sAllParams = @sAllParams + ', '
			SET @sAllFields = @sAllFields + ', '
		 END

		IF (@sTypeName = 'timestamp')
			SET @sAllParams = @sAllParams + 'NULL'
		ELSE IF (@sDefaultValue IS NOT NULL)
			SET @sAllParams = @sAllParams + 'COALESCE(@' + @sColumnName + ', ' + @sDefaultValue + ')'
		ELSE
			SET @sAllParams = @sAllParams + '@' + @sColumnName 

		SET @sAllFields = @sAllFields + @sColumnName 

	 END
	ELSE
	 BEGIN
		SET @HasIdentity = 1
	 END

	IF (@nAlternateType = 2) --decimal, numeric
		SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnPrecision AS varchar(3)) + ', ' 
				+ CAST(@nColumnScale AS varchar(3)) + ')'

	ELSE IF (@nAlternateType = 1) --character and binary
		SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnLength AS varchar(4)) +  ')'

	IF (@IsIdentity = 0)
	 BEGIN
		IF (@sDefaultValue IS NOT NULL) OR (@IsNullable = 1) OR (@sTypeName = 'timestamp')
			SET @sKeyFields = @sKeyFields + ' = NULL'
	 END

	FETCH 	NEXT 
	FROM 	crKeyFields 
	INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
		@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
		@IsIdentity, @sTypeName, @sDefaultValue
 END

CLOSE crKeyFields
DEALLOCATE crKeyFields

SET 	@sProcText = @sProcText + @sKeyFields + @sCRLF
SET 	@sProcText = @sProcText + 'AS' + @sCRLF
SET 	@sProcText = @sProcText + @sCRLF
SET 	@sProcText = @sProcText + 'INSERT ' + @sTableName + '(' + @sAllFields + ')' + @sCRLF
SET 	@sProcText = @sProcText + 'VALUES (' + @sAllParams + ')' + @sCRLF
SET 	@sProcText = @sProcText + @sCRLF

IF (@HasIdentity = 1)
 BEGIN
	SET 	@sProcText = @sProcText + 'RETURN SCOPE_IDENTITY()' + @sCRLF
	SET 	@sProcText = @sProcText + @sCRLF
 END

IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF


PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)
GO
/****** Object:  StoredProcedure [dbo].[pr__SYS_MakeDeleteRecordProc]      ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO
CREATE      PROC [dbo].[pr__SYS_MakeDeleteRecordProc]
	@sTableName varchar(128),
	@bExecute bit = 0
AS

IF dbo.fnTableHasPrimaryKey(@sTableName) = 0
 BEGIN
	RAISERROR ('Procedure cannot be created on a table with no primary key.', 10, 1)
	RETURN
 END

DECLARE	@sProcText varchar(8000),
	@sKeyFields varchar(2000),
	@sWhereClause varchar(2000),
	@sColumnName varchar(128),
	@nColumnID smallint,
	@bPrimaryKeyColumn bit,
	@nAlternateType int,
	@nColumnLength int,
	@nColumnPrecision int,
	@nColumnScale int,
	@IsNullable bit, 
	@IsIdentity int,
	@sTypeName varchar(128),
	@sDefaultValue varchar(4000),
	@sCRLF char(2),
	@sTAB char(1)

SET	@sTAB = char(9)
SET 	@sCRLF = char(13) + char(10)

SET 	@sProcText = ''
SET 	@sKeyFields = ''
SET	@sWhereClause = ''

SET 	@sProcText = @sProcText + 'IF EXISTS(SELECT * FROM sysobjects WHERE name = ''prApp_' + @sTableName + '_Delete'')' + @sCRLF
SET 	@sProcText = @sProcText + @sTAB + 'DROP PROC prApp_' + @sTableName + '_Delete' + @sCRLF
IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF

SET 	@sProcText = @sProcText + @sCRLF

PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)

SET 	@sProcText = ''
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + '-- Delete a single record from ' + @sTableName + @sCRLF
SET 	@sProcText = @sProcText + '----------------------------------------------------------------------------' + @sCRLF
SET 	@sProcText = @sProcText + 'CREATE PROC prApp_' + @sTableName + '_Delete' + @sCRLF

DECLARE crKeyFields cursor for
	SELECT	*
	FROM	dbo.fnTableColumnInfo(@sTableName)
	ORDER BY 2

OPEN crKeyFields

FETCH 	NEXT 
FROM 	crKeyFields 
INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
	@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
	@IsIdentity, @sTypeName, @sDefaultValue
				
WHILE (@@FETCH_STATUS = 0)
 BEGIN

	IF (@bPrimaryKeyColumn = 1)
	 BEGIN
		IF (@sKeyFields <> '')
			SET @sKeyFields = @sKeyFields + ',' + @sCRLF 
	
		SET @sKeyFields = @sKeyFields + @sTAB + '@' + @sColumnName + ' ' + @sTypeName

		IF (@nAlternateType = 2) --decimal, numeric
			SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnPrecision AS varchar(3)) + ', ' 
					+ CAST(@nColumnScale AS varchar(3)) + ')'
	
		ELSE IF (@nAlternateType = 1) --character and binary
			SET @sKeyFields =  @sKeyFields + '(' + CAST(@nColumnLength AS varchar(4)) +  ')'
	
		IF (@sWhereClause = '')
			SET @sWhereClause = @sWhereClause + 'WHERE ' 
		ELSE
			SET @sWhereClause = @sWhereClause + ' AND ' 

		SET @sWhereClause = @sWhereClause + @sTAB + @sColumnName  + ' = @' + @sColumnName + @sCRLF 
	 END

	FETCH 	NEXT 
	FROM 	crKeyFields 
	INTO 	@sColumnName, @nColumnID, @bPrimaryKeyColumn, @nAlternateType, 
		@nColumnLength, @nColumnPrecision, @nColumnScale, @IsNullable, 
		@IsIdentity, @sTypeName, @sDefaultValue
 END

CLOSE crKeyFields
DEALLOCATE crKeyFields

SET 	@sProcText = @sProcText + @sKeyFields + @sCRLF
SET 	@sProcText = @sProcText + 'AS' + @sCRLF
SET 	@sProcText = @sProcText + @sCRLF
SET 	@sProcText = @sProcText + 'DELETE	' + @sTableName + @sCRLF
SET 	@sProcText = @sProcText + @sWhereClause
SET 	@sProcText = @sProcText + @sCRLF
IF @bExecute = 0
	SET 	@sProcText = @sProcText + 'GO' + @sCRLF


PRINT @sProcText

IF @bExecute = 1 
	EXEC (@sProcText)
GO
/****** Object:  Default [DF_Zones_Height]     ******/
ALTER TABLE [dbo].[Zones] ADD  CONSTRAINT [DF_Zones_Height]  DEFAULT (0) FOR [Height]
GO
/****** Object:  Default [DF_World_name]     ******/
ALTER TABLE [dbo].[World] ADD  CONSTRAINT [DF_World_name]  DEFAULT (N'Dragon''s Spine') FOR [name]
GO
/****** Object:  Default [DF_World_lottery]     ******/
ALTER TABLE [dbo].[World] ADD  CONSTRAINT [DF_World_lottery]  DEFAULT ((0)) FOR [lottery]
GO
/****** Object:  Default [DF_Quest_totalSteps]      ******/
ALTER TABLE [dbo].[Quest] ADD  CONSTRAINT [DF_Quest_totalSteps]  DEFAULT ((1)) FOR [totalSteps]
GO
/****** Object:  Default [DF_Quest_despawnsNPC]      ******/
ALTER TABLE [dbo].[Quest] ADD  CONSTRAINT [DF_Quest_despawnsNPC]  DEFAULT ((0)) FOR [despawnsNPC]
GO
/****** Object:  Default [DF_Quest_ownerQuestID]      ******/
ALTER TABLE [dbo].[Quest] ADD  CONSTRAINT [DF_Quest_ownerQuestID]  DEFAULT ((0)) FOR [masterQuestID]
GO
/****** Object:  Default [DF_Quest_teleportGroup]      ******/
ALTER TABLE [dbo].[Quest] ADD  CONSTRAINT [DF_Quest_teleportGroup]  DEFAULT ((0)) FOR [teleportGroup]
GO
/****** Object:  Default [DF_CharGen_Fighter_Race]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_Fighter_Race]  DEFAULT ('Illyria') FOR [race]
GO
/****** Object:  Default [DF_CharGen_Fighter_Class]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_Fighter_Class]  DEFAULT ('Fighter') FOR [class]
GO
/****** Object:  Default [DF_CharGen_classType]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_classType]  DEFAULT ((0)) FOR [classType]
GO
/****** Object:  Default [DF_CharGen_Alignment]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_Alignment]  DEFAULT ((0)) FOR [alignment]
GO
/****** Object:  Default [DF_CharGenFighter_Bow]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Bow]  DEFAULT ((0)) FOR [bow]
GO
/****** Object:  Default [DF_CharGenFighter_Dagger]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Dagger]  DEFAULT ((0)) FOR [dagger]
GO
/****** Object:  Default [DF_CharGenFighter_Flail]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Flail]  DEFAULT ((0)) FOR [flail]
GO
/****** Object:  Default [DF_CharGenFighter_Halberd]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Halberd]  DEFAULT ((0)) FOR [halberd]
GO
/****** Object:  Default [DF_CharGenFighter_Mace]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Mace]  DEFAULT ((0)) FOR [mace]
GO
/****** Object:  Default [DF_CharGenFighter_Rapier]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Rapier]  DEFAULT ((0)) FOR [rapier]
GO
/****** Object:  Default [DF_CharGenFighter_Shuriken]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Shuriken]  DEFAULT ((0)) FOR [shuriken]
GO
/****** Object:  Default [DF_CharGenFighter_Staff]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Staff]  DEFAULT ((0)) FOR [staff]
GO
/****** Object:  Default [DF_CharGenFighter_Sword]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Sword]  DEFAULT ((0)) FOR [sword]
GO
/****** Object:  Default [DF_CharGenFighter_Threestaff]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Threestaff]  DEFAULT ((0)) FOR [threestaff]
GO
/****** Object:  Default [DF_CharGenFighter_Twohanded]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Twohanded]  DEFAULT ((0)) FOR [twoHanded]
GO
/****** Object:  Default [DF_CharGenFighter_Thievery]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Thievery]  DEFAULT ((0)) FOR [thievery]
GO
/****** Object:  Default [DF_CharGenFighter_Magic]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Magic]  DEFAULT ((0)) FOR [magic]
GO
/****** Object:  Default [DF_CharGenFighter_Unarmed]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGenFighter_Unarmed]  DEFAULT ((0)) FOR [unarmed]
GO
/****** Object:  Default [DF_CharGen_bash]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_bash]  DEFAULT ((0)) FOR [bash]
GO
/****** Object:  Default [DF_CharGen_RightHand]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_RightHand]  DEFAULT ((0)) FOR [rightHand]
GO
/****** Object:  Default [DF_CharGen_LeftHand]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_LeftHand]  DEFAULT ((0)) FOR [leftHand]
GO
/****** Object:  Default [DF_CharGen_Wearing]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_Wearing]  DEFAULT ((0)) FOR [wearing]
GO
/****** Object:  Default [DF_CharGen_Sack]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_Sack]  DEFAULT ((0)) FOR [sack]
GO
/****** Object:  Default [DF_CharGen_Belt]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_Belt]  DEFAULT ((0)) FOR [belt]
GO
/****** Object:  Default [DF_CharGen_Spells]      ******/
ALTER TABLE [dbo].[CharGen] ADD  CONSTRAINT [DF_CharGen_Spells]  DEFAULT ((0)) FOR [spells]
GO
/****** Object:  Default [DF_Cell_mailbox]      ******/
ALTER TABLE [dbo].[Cell] ADD  CONSTRAINT [DF_Cell_mailbox]  DEFAULT ((0)) FOR [mailbox]
GO
/****** Object:  Default [DF_CatalogItem_visualKey_1]      ******/
ALTER TABLE [dbo].[CatalogItem] ADD  CONSTRAINT [DF_CatalogItem_visualKey_1]  DEFAULT ('unknown') FOR [visualKey]
GO
/****** Object:  Default [DF_CatalogItem_weight]      ******/
ALTER TABLE [dbo].[CatalogItem] ADD  CONSTRAINT [DF_CatalogItem_weight]  DEFAULT ((0)) FOR [weight]
GO
/****** Object:  Default [DF_CatalogItem_attuneType]      ******/
ALTER TABLE [dbo].[CatalogItem] ADD  CONSTRAINT [DF_CatalogItem_attuneType]  DEFAULT ('None') FOR [attuneType]
GO
/****** Object:  Default [DF_Account_CurrentMarks_1]      ******/
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_CurrentMarks_1]  DEFAULT ((0)) FOR [currentMarks]
GO
/****** Object:  Default [DF_Account_LifetimeMarks]      ******/
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_LifetimeMarks]  DEFAULT ((0)) FOR [lifetimeMarks]
GO
/****** Object:  Default [DF_Account_LastOnline]      ******/
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_LastOnline]  DEFAULT ('') FOR [lastOnline]
GO
/****** Object:  Default [DF_Account_banLength]      ******/
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_banLength]  DEFAULT ((0)) FOR [banLength]
GO
/****** Object:  Default [DF_Account_email]      ******/
ALTER TABLE [dbo].[Account] ADD  CONSTRAINT [DF_Account_email]  DEFAULT ('') FOR [email]
GO
/****** Object:  Default [DF_NPC_thac0Adjustment]      ******/
ALTER TABLE [dbo].[NPC] ADD  CONSTRAINT [DF_NPC_thac0Adjustment]  DEFAULT ((0)) FOR [thac0Adjustment]
GO
/****** Object:  Default [DF_NPC_bash]      ******/
ALTER TABLE [dbo].[NPC] ADD  CONSTRAINT [DF_NPC_bash]  DEFAULT ((0)) FOR [bash]
GO
/****** Object:  Default [DF_Map_randomMagicIntensity]      ******/
ALTER TABLE [dbo].[Map] ADD  CONSTRAINT [DF_Map_randomMagicIntensity]  DEFAULT ((0)) FOR [randomMagicIntensity]
GO
/****** Object:  Default [DF__LivePC__isDead__53E3F47C]      ******/
ALTER TABLE [dbo].[LivePC] ADD  DEFAULT ((0)) FOR [isDead]
GO
/****** Object:  Default [DF__LiveNPC__isDead__510787D1]      ******/
ALTER TABLE [dbo].[LiveNPC] ADD  DEFAULT ((0)) FOR [isDead]
GO
/****** Object:  Default [DF_Land_statCapOperand]      ******/
ALTER TABLE [dbo].[Land] ADD  CONSTRAINT [DF_Land_statCapOperand]  DEFAULT ((0)) FOR [statCapOperand]
GO
/****** Object:  Default [DF_Land_maximumAbilityScore]      ******/
ALTER TABLE [dbo].[Land] ADD  CONSTRAINT [DF_Land_maximumAbilityScore]  DEFAULT ((18)) FOR [maximumAbilityScore]
GO
/****** Object:  Default [DF_Land_maximumShielding]      ******/
ALTER TABLE [dbo].[Land] ADD  CONSTRAINT [DF_Land_maximumShielding]  DEFAULT ((9)) FOR [maximumShielding]
GO
/****** Object:  Default [DF_Land_maximumTempAbilityScore]      ******/
ALTER TABLE [dbo].[Land] ADD  CONSTRAINT [DF_Land_maximumTempAbilityScore]  DEFAULT ((18)) FOR [maximumTempAbilityScore]
GO
/****** Object:  Default [DF_PlayerWearing_attunedID]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_attunedID]  DEFAULT ((0)) FOR [attunedID]
GO
/****** Object:  Default [DF_PlayerWearing_wearOrientation]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_wearOrientation]  DEFAULT ((0)) FOR [wearOrientation]
GO
/****** Object:  Default [DF_PlayerWearing_special]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_special]  DEFAULT ('') FOR [special]
GO
/****** Object:  Default [DF_PlayerWearing_CoinValue_1]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_CoinValue_1]  DEFAULT ((0)) FOR [coinvalue]
GO
/****** Object:  Default [DF_PlayerWearing_Charges_1]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_Charges_1]  DEFAULT ((0)) FOR [charges]
GO
/****** Object:  Default [DF_PlayerWearing_WillAttune_1]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_WillAttune_1]  DEFAULT ((0)) FOR [attuneType]
GO
/****** Object:  Default [DF_PlayerWearing_TimeCreated]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_TimeCreated]  DEFAULT ('1/1/2006 12:00:00 AM') FOR [timeCreated]
GO
/****** Object:  Default [DF_PlayerWearing_WhoCreated]      ******/
ALTER TABLE [dbo].[PlayerWearing] ADD  CONSTRAINT [DF_PlayerWearing_WhoCreated]  DEFAULT (N'SYSTEM') FOR [whoCreated]
GO
/****** Object:  Default [DF_PlayerSettings_anonymous]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_anonymous]  DEFAULT ((0)) FOR [anonymous]
GO
/****** Object:  Default [DF_PlayerSettings_echo]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_echo]  DEFAULT ((1)) FOR [echo]
GO
/****** Object:  Default [DF_PlayerSettings_filterProfanity]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_filterProfanity]  DEFAULT ((1)) FOR [filterProfanity]
GO
/****** Object:  Default [DF_PlayerSettings_friendsList]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_friendsList]  DEFAULT ((0)) FOR [friendsList]
GO
/****** Object:  Default [DF_PlayerSettings_friendNotify]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_friendNotify]  DEFAULT ((0)) FOR [friendNotify]
GO
/****** Object:  Default [DF_PlayerSettings_ignoreList]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_ignoreList]  DEFAULT ((0)) FOR [ignoreList]
GO
/****** Object:  Default [DF_PlayerSettings_immortal]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_immortal]  DEFAULT ((0)) FOR [immortal]
GO
/****** Object:  Default [DF_PlayerSettings_invisible]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_invisible]  DEFAULT ((0)) FOR [invisible]
GO
/****** Object:  Default [DF_PlayerSettings_receiveGroupInvites]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_receiveGroupInvites]  DEFAULT ((0)) FOR [receiveGroupInvites]
GO
/****** Object:  Default [DF_PlayerSettings_receivePages]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_receivePages]  DEFAULT ((1)) FOR [receivePages]
GO
/****** Object:  Default [DF_PlayerSettings_receiveTells]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_receiveTells]  DEFAULT ((1)) FOR [receiveTells]
GO
/****** Object:  Default [DF_PlayerSettings_showStaffTitle]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_showStaffTitle]  DEFAULT ((1)) FOR [showStaffTitle]
GO
/****** Object:  Default [DF_PlayerSettings_displayCombatDamage]      ******/
ALTER TABLE [dbo].[PlayerSettings] ADD  CONSTRAINT [DF_PlayerSettings_displayCombatDamage]  DEFAULT ((0)) FOR [displayCombatDamage]
GO
/****** Object:  Default [DF_PlayerSack_Special]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_Special]  DEFAULT ((0)) FOR [Special]
GO
/****** Object:  Default [DF_PlayerSack_CoinValue]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_CoinValue]  DEFAULT ((0)) FOR [CoinValue]
GO
/****** Object:  Default [DF_PlayerSack_Charges]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_Charges]  DEFAULT ((0)) FOR [Charges]
GO
/****** Object:  Default [DF_PlayerSack_Venom]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_Venom]  DEFAULT ((0)) FOR [Venom]
GO
/****** Object:  Default [DF_PlayerSack_WillAttune_1]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_WillAttune_1]  DEFAULT ((0)) FOR [attuneType]
GO
/****** Object:  Default [DF_PlayerSack_FigExp]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_FigExp]  DEFAULT ((0)) FOR [FigExp]
GO
/****** Object:  Default [DF_PlayerSack_TimeCreated]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_TimeCreated]  DEFAULT ('1/1/2006 12:00:00 AM') FOR [TimeCreated]
GO
/****** Object:  Default [DF_PlayerSack_WhoCreated]      ******/
ALTER TABLE [dbo].[PlayerSack] ADD  CONSTRAINT [DF_PlayerSack_WhoCreated]  DEFAULT (N'SYSTEM') FOR [WhoCreated]
GO
/****** Object:  Default [DF_PlayerRings_wasRecall_1]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_wasRecall_1]  DEFAULT ((0)) FOR [wasRecall]
GO
/****** Object:  Default [DF_PlayerRings_recallZ]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_recallZ]  DEFAULT ((0)) FOR [recallZ]
GO
/****** Object:  Default [DF_PlayerRings_Special_1]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_Special_1]  DEFAULT ((0)) FOR [special]
GO
/****** Object:  Default [DF_PlayerRings_CoinValue_1]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_CoinValue_1]  DEFAULT ((0)) FOR [coinValue]
GO
/****** Object:  Default [DF_PlayerRings_Charges_1]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_Charges_1]  DEFAULT ((0)) FOR [charges]
GO
/****** Object:  Default [DF_PlayerRings_WillAttune_1]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_WillAttune_1]  DEFAULT ((0)) FOR [attuneType]
GO
/****** Object:  Default [DF_PlayerRings_TimeCreated]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_TimeCreated]  DEFAULT ('1/1/2005 12:00:00 AM') FOR [timeCreated]
GO
/****** Object:  Default [DF_PlayerRings_WhoCreated]      ******/
ALTER TABLE [dbo].[PlayerRings] ADD  CONSTRAINT [DF_PlayerRings_WhoCreated]  DEFAULT (N'SYSTEM') FOR [whoCreated]
GO
/****** Object:  Default [DF_PlayerQuests_timesCompleted]      ******/
ALTER TABLE [dbo].[PlayerQuests] ADD  CONSTRAINT [DF_PlayerQuests_timesCompleted]  DEFAULT ((0)) FOR [timesCompleted]
GO
/****** Object:  Default [DF_PlayerQuests_currentStep]      ******/
ALTER TABLE [dbo].[PlayerQuests] ADD  CONSTRAINT [DF_PlayerQuests_currentStep]  DEFAULT ((0)) FOR [currentStep]
GO
/****** Object:  Default [DF_PlayerLocker_Special_1]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_Special_1]  DEFAULT ((0)) FOR [special]
GO
/****** Object:  Default [DF_PlayerLocker_Charges_1]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_Charges_1]  DEFAULT ((0)) FOR [charges]
GO
/****** Object:  Default [DF_PlayerLocker_CoinValue_1]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_CoinValue_1]  DEFAULT ((0)) FOR [coinValue]
GO
/****** Object:  Default [DF_PlayerLocker_venom]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_venom]  DEFAULT ((0)) FOR [venom]
GO
/****** Object:  Default [DF_PlayerLocker_FigExp]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_FigExp]  DEFAULT ((0)) FOR [figExp]
GO
/****** Object:  Default [DF_PlayerLocker_WillAttune_1]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_WillAttune_1]  DEFAULT ((0)) FOR [attuneType]
GO
/****** Object:  Default [DF_PlayerLocker_nocked]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_nocked]  DEFAULT ((0)) FOR [nocked]
GO
/****** Object:  Default [DF_PlayerLocker_TimeCreated]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_TimeCreated]  DEFAULT ('1/1/2006 12:00:00 AM') FOR [timeCreated]
GO
/****** Object:  Default [DF_PlayerLocker_WhoCreated]      ******/
ALTER TABLE [dbo].[PlayerLocker] ADD  CONSTRAINT [DF_PlayerLocker_WhoCreated]  DEFAULT (N'SYSTEM') FOR [whoCreated]
GO
/****** Object:  Default [DF_PlayerHeld_attuneType]      ******/
ALTER TABLE [dbo].[PlayerHeld] ADD  CONSTRAINT [DF_PlayerHeld_attuneType]  DEFAULT ((0)) FOR [attuneType]
GO
/****** Object:  Default [DF_PlayerHeld_nocked]      ******/
ALTER TABLE [dbo].[PlayerHeld] ADD  CONSTRAINT [DF_PlayerHeld_nocked]  DEFAULT ((0)) FOR [nocked]
GO
/****** Object:  Default [DF_PlayerBelt_Special_1]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_Special_1]  DEFAULT ((0)) FOR [special]
GO
/****** Object:  Default [DF_PlayerBelt_CoinValue_1]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_CoinValue_1]  DEFAULT ((0)) FOR [coinValue]
GO
/****** Object:  Default [DF_PlayerBelt_Charges_1]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_Charges_1]  DEFAULT ((0)) FOR [charges]
GO
/****** Object:  Default [DF_PlayerBelt_Venom_1]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_Venom_1]  DEFAULT ((0)) FOR [venom]
GO
/****** Object:  Default [DF_PlayerBelt_WillAttune_1]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_WillAttune_1]  DEFAULT ((0)) FOR [attuneType]
GO
/****** Object:  Default [DF_PlayerBelt_FigExp]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_FigExp]  DEFAULT ((0)) FOR [figExp]
GO
/****** Object:  Default [DF_PlayerBelt_nocked]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_nocked]  DEFAULT ((0)) FOR [nocked]
GO
/****** Object:  Default [DF_PlayerBelt_TimeCreated_1]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_TimeCreated_1]  DEFAULT ('1/1/2006 12:00:00 AM') FOR [timeCreated]
GO
/****** Object:  Default [DF_PlayerBelt_WhoCreated]      ******/
ALTER TABLE [dbo].[PlayerBelt] ADD  CONSTRAINT [DF_PlayerBelt_WhoCreated]  DEFAULT (N'SYSTEM') FOR [whoCreated]
GO
/****** Object:  Default [DF_Player_ConfRoom]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_ConfRoom]  DEFAULT ((1)) FOR [confRoom]
GO
/****** Object:  Default [DF_Player_Ancestor]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_Ancestor]  DEFAULT ((0)) FOR [ancestor]
GO
/****** Object:  Default [DF_Player_AncestorID]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_AncestorID]  DEFAULT ((0)) FOR [ancestorID]
GO
/****** Object:  Default [DF_Player_facet]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_facet]  DEFAULT ((0)) FOR [facet]
GO
/****** Object:  Default [DF_Player_zCord]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_zCord]  DEFAULT ((0)) FOR [zCord]
GO
/****** Object:  Default [DF_Player_fighterSpecialization_1]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_fighterSpecialization_1]  DEFAULT ('None') FOR [fighterSpecialization]
GO
/****** Object:  Default [DF_Player_hitsAdjustment]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_hitsAdjustment]  DEFAULT ((0)) FOR [hitsAdjustment]
GO
/****** Object:  Default [DF_Player_hitsDoctored]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_hitsDoctored]  DEFAULT ((0)) FOR [hitsDoctored]
GO
/****** Object:  Default [DF_Player_staminaAdjustment]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_staminaAdjustment]  DEFAULT ((0)) FOR [staminaAdjustment]
GO
/****** Object:  Default [DF_Player_manaAdjustment]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_manaAdjustment]  DEFAULT ((0)) FOR [manaAdjustment]
GO
/****** Object:  Default [DF_Player_RoundsPlayed]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_RoundsPlayed]  DEFAULT ((0)) FOR [roundsPlayed]
GO
/****** Object:  Default [DF_Player_birthday]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_birthday]  DEFAULT ('3/28/2006 12:00:00 AM') FOR [birthday]
GO
/****** Object:  Default [DF_Player_lastOnline]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_lastOnline]  DEFAULT ('3/28/2006 12:00:00 AM') FOR [lastOnline]
GO
/****** Object:  Default [DF_Player_deleteDate]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_deleteDate]  DEFAULT ('3/28/2006 12:00:00 AM') FOR [deleteDate]
GO
/****** Object:  Default [DF_Player_CurrentKarma]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_CurrentKarma]  DEFAULT ((0)) FOR [currentKarma]
GO
/****** Object:  Default [DF_Player_LifetimeKarma]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_LifetimeKarma]  DEFAULT ((0)) FOR [lifetimeKarma]
GO
/****** Object:  Default [DF_Player_LifetimeMarks]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_LifetimeMarks]  DEFAULT ((0)) FOR [lifetimeMarks]
GO
/****** Object:  Default [DF_Player_PvPKills]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_PvPKills]  DEFAULT ((0)) FOR [pvpKills]
GO
/****** Object:  Default [DF_Player_PvPDeaths]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_PvPDeaths]  DEFAULT ((0)) FOR [pvpDeaths]
GO
/****** Object:  Default [DF_Player_UW_HitsMax]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_HitsMax]  DEFAULT ((0)) FOR [UW_hitsMax]
GO
/****** Object:  Default [DF_Player_UW_hitsAdjustment]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_hitsAdjustment]  DEFAULT ((0)) FOR [UW_hitsAdjustment]
GO
/****** Object:  Default [DF_Player_UW_Stamina]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_Stamina]  DEFAULT ((0)) FOR [UW_staminaMax]
GO
/****** Object:  Default [DF_Player_UW_staminaAdjustment]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_staminaAdjustment]  DEFAULT ((0)) FOR [UW_staminaAdjustment]
GO
/****** Object:  Default [DF_Player_UW_ManaMax]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_ManaMax]  DEFAULT ((0)) FOR [UW_manaMax]
GO
/****** Object:  Default [DF_Player_UW_manaAdjustment]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_manaAdjustment]  DEFAULT ((0)) FOR [UW_manaAdjustment]
GO
/****** Object:  Default [DF_Player_UW_HasIntestines]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_HasIntestines]  DEFAULT ((0)) FOR [UW_intestines]
GO
/****** Object:  Default [DF_Player_UW_HasLiver]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_HasLiver]  DEFAULT ((0)) FOR [UW_liver]
GO
/****** Object:  Default [DF_Player_UW_HasLungs]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_HasLungs]  DEFAULT ((0)) FOR [UW_lungs]
GO
/****** Object:  Default [DF_Player_UW_HasStomach]      ******/
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_UW_HasStomach]  DEFAULT ((0)) FOR [UW_stomach]
GO


/* Log stuff */

/****** Object:  Table [dbo].[Log] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Log]') AND type in (N'U'))
DROP TABLE [dbo].[Log]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[logtime] [datetime] NOT NULL,
	[logtype] [nvarchar](50) NULL,
	[message] [nvarchar](max) NULL,
 CONSTRAINT [PK__Log__03A67F89] PRIMARY KEY NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Log] ADD  CONSTRAINT [DF_Log_LogTime]  DEFAULT (getdate()) FOR [logtime]
GO

/****** Object:  StoredProcedure [dbo].[prApp_Log_Add] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_Log_Add]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_Log_Add]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[prApp_Log_Add] 
	@logtype nvarchar(50) = '', 
	@message nvarchar(MAX) = ''
AS
	INSERT Log(logtype, message)
	VALUES (@logtype, @message);	
GO

/****** Object:  StoredProcedure [dbo].[prApp_Log_By_MinID] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_Log_By_MinID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_Log_By_MinID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[prApp_Log_By_MinID]
	@minId int
AS
SELECT	*
FROM	Log
WHERE id >= @minId
ORDER BY id
GO

/****** Object:  StoredProcedure [dbo].[prApp_Log_Get_MaxID] ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[prApp_Log_Get_MaxID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[prApp_Log_Get_MaxID]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[prApp_Log_Get_MaxID]
AS
SELECT TOP 1 *
  FROM Log
  ORDER BY id DESC
GO

/* end log stuff */

USE [master]
GO
GRANT CONNECT TO [db1_readonly] AS [dbo]
GO
USE [$(MYDATABASE)]
GO
GRANT EXECUTE ON [dbo].[prApp_NPCLocation_By_MapID] TO [db1_readonly] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[prApp_LivePC_By_MapID] TO [db1_readonly] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[prApp_LiveNPC_By_MapID] TO [db1_readonly] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[prApp_LiveCell_By_MapID] TO [db1_readonly] AS [dbo]
GO
GRANT EXECUTE ON [dbo].[prApp_Cell_Select_By_Map] TO [db1_readonly] AS [dbo]
GO
GRANT VIEW DEFINITION ON [dbo].[prApp_Cell_Select_By_Map] TO [db1_readonly] AS [dbo]
GO



# mssql-dragspinexp

## Name

Microsoft SQL container for use with drag-spin-exp.

## Description

A portable way to deploy a SQL database to use with the game server in
[drag-spin-exp](https://gitlab.com/LiBiPlJoe/drag-spin-exp). Includes a `minimal` database for
testing, and a `production` database for running the full game.

## Usage

### Building the container

``` text
docker build -t mssql-dragspinexp --no-cache=true --progress=plain . 2>&1 | tee build.log
```

This image will have the `sa` user's password set to `TempBadPassw0rd`. Obviously it should be
changed immediately when running the image.

### Running the container

The `SQLSERVR_SA_PASSWORD` environment variable will be used to set the password for the `sa` user.
If it is not already present, create the `drag-spin-exp` network.

``` text
export SQLSERVR_SA_PASSWORD=StrongPassw0rd
docker run -e 'ACCEPT_EULA=Y' -e SQLSERVR_SA_PASSWORD -p 1433:1433 --name sql --net drag-spin-exp -d mssql-dragspinexp
```

## Authors and acknowledgment

### Origins

Based largely on [Microsoft's mssql-docker repo](https://github.com/microsoft/mssql-docker),
specifically the [mssql-customize](https://github.com/microsoft/mssql-docker/tree/master/linux/preview/examples/mssql-customize)
example.

## License

The Docker resource files for SQL Server are licensed under the MIT license.  See the [LICENSE file](LICENSE) for more details.

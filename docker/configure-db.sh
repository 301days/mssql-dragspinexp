#!/bin/bash

# Start SQL Server
echo "Starting SQL Server..."
/opt/mssql/bin/sqlservr &
sleep 1

# Wait 60 seconds for SQL Server to start up by ensuring that
# calling SQLCMD does not return an error code, which will ensure that sqlcmd is accessible
# and that system and user databases return "0" which means all databases are in an "online" state
# https://docs.microsoft.com/en-us/sql/relational-databases/system-catalog-views/sys-databases-transact-sql?view=sql-server-2017

DBSTATUS=1
ERRCODE=1
i=0

while [[ $((DBSTATUS + ERRCODE)) -ne 0 ]] && [[ $i -le 60 ]]; do
	i=$(( i + 1 ))
	echo "Checking server status ($i of 60)..."
	DBSTATUS=$(/opt/mssql-tools/bin/sqlcmd -h -1 -t 1 -U sa -P $SA_PASSWORD -Q "SET NOCOUNT ON; Select SUM(state) from sys.databases")
	ERRCODE=$?
	sleep 1
done

echo $DBSTATUS
echo $ERRCODE
if [[ $DBSTATUS -ne 0 ]] || [[ $ERRCODE -ne 0 ]]; then
	echo "SQL Server took more than 60 seconds to start up or one or more databases are not in an ONLINE state"
	echo "Server logs:"
	ls /var/opt/mssql/log/*
	cat /var/opt/mssql/log/*.log
	cat /var/opt/mssql/log/*.json
	cat /var/opt/mssql/log/*.txt
	echo "End server logs."
	exit 1
fi

# Run the setup script to create the DB and the schema in the DB
echo "Adding production database..."
/opt/mssql-tools/bin/sqlcmd -b -S localhost -U sa -P $SA_PASSWORD -d master -v MYDATABASE="production" -i EntireDB-production.sql > production.out
if [[ $ERRCODE -ne 0 ]]; then
	echo "SQLCMD errcode $ERRCODE adding production database!"
	exit 1
fi

echo "Adding minimal database..."
/opt/mssql-tools/bin/sqlcmd -b -S localhost -U sa -P $SA_PASSWORD -d master -v MYDATABASE="minimal" -i EntireDB-minimal.sql > minimal.out
if [[ $ERRCODE -ne 0 ]]; then
	echo "SQLCMD errcode $ERRCODE adding minimal database!"
	exit 1
fi

echo "Databases added"
sleep 1
echo "Shutting down the database..."
/opt/mssql-tools/bin/sqlcmd -b -S localhost -U sa -P $SA_PASSWORD -Q "SHUTDOWN"
if [[ $ERRCODE -ne 0 ]]; then
	echo "SQLCMD errcode $ERRCODE shutting down database!"
	exit 1
fi
sleep 1
echo "Done"

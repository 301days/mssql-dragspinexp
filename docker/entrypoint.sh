#!/bin/bash

# Start SQL Server with password reset to SQLSERVR_SA_PASSWORD
/opt/mssql/bin/sqlservr --reset-sa-password

tail -f /dev/null
